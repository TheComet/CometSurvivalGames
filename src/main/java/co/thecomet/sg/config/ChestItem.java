package co.thecomet.sg.config;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChestItem {
    public String type;
    public byte data;
    public short durability;
    public int minAmount = 1;
    public int maxAmount = 1;
    public String displayName;
    public List<String> lore;
    public Map<String, Integer> enchantments;
    public List<ChestItemPotionEffect> potionEffects;
    public int requiredChestPoints = 5;
    public double rarity = 100;
    
    public ChestItem(ItemStack item, int min, int max, int requiredChestPoints, double rarity) {
        this(item, min, max);
        this.requiredChestPoints = requiredChestPoints > 0 ? requiredChestPoints : 5;
        this.rarity = rarity > 1.0 ? 1.0 : rarity > 0.0 ? rarity : 1.0;
    }
    
    public ChestItem(ItemStack item, int min, int max) {
        this(item);
        this.minAmount = min;
        this.maxAmount = max;
    }
    
    public ChestItem(ItemStack item) {
        this.type = item.getType().name();
        this.data = item.getData().getData();
        this.durability = item.getDurability();
        
        if (item.getItemMeta().hasDisplayName()) {
            displayName = item.getItemMeta().getDisplayName();
        }
        
        if (item.getItemMeta().hasLore()) {
            lore = item.getItemMeta().getLore();
        }
        
        if (item.getEnchantments().isEmpty() == false) {
            enchantments = new HashMap<>();
            for (Map.Entry<Enchantment, Integer> enchantment : item.getEnchantments().entrySet()) {
                enchantments.put(enchantment.getKey().getName(), enchantment.getValue());
            }
        }
        
        if (item.getType() == Material.POTION) {
            if (Potion.fromItemStack(item).getEffects().isEmpty() == false) {
                potionEffects = new ArrayList<>();
                for (PotionEffect effect : Potion.fromItemStack(item).getEffects()) {
                    potionEffects.add(new ChestItemPotionEffect(effect));
                }
            }
        }
    }
    
    public ItemStack getItemStack() {
        ItemBuilder builder = ItemBuilder.build(getMaterial());
        if (data > -1) {
            builder.data(data);
        }
        
        if (durability > -1) {
            builder.durability(durability);
        }
        
        if (enchantments != null && enchantments.isEmpty() == false) {
            enchantments.forEach((enchant, level) -> {
                Enchantment enchantment = getEnchantment(enchant);
                
                if (enchantment != null) {
                    builder.enchantment(enchantment, level);
                }
            });
        }
        
        builder.amount(maxAmount <= minAmount ? minAmount : GeneralUtils.getBetween(minAmount, maxAmount));
        
        if (displayName != null) {
            builder.name(FontColor.translateString(displayName));
        }
        
        if (lore != null && lore.isEmpty() == false) {
            lore.forEach(line -> builder.lore(FontColor.translateString(line)));
        }
        
        if (potionEffects != null && potionEffects.isEmpty() == false) {
            potionEffects.forEach(effect -> {
                PotionEffect e = effect.getPotionEffect();
                if (e != null) {
                    builder.effect(e);
                }
            });
        }
        
        return builder.build();
    }
    
    public Material getMaterial() {
        return Material.valueOf(type);
    }
    
    public Enchantment getEnchantment(String enchant) {
        return Enchantment.getByName(enchant);
    }
    
    public class ChestItemPotionEffect {
        String type;
        int duration;
        int amplifier;
        boolean ambient;
        
        public ChestItemPotionEffect(PotionEffect effect) {
            this.type = effect.getType().getName();
            this.duration = effect.getDuration();
            this.amplifier = effect.getAmplifier();
            this.ambient = effect.isAmbient();
        }
        
        public PotionEffect getPotionEffect() {
            if (this.type == null) {
                return null;
            }
            
            PotionEffectType type = getPotionEffectType();
            
            if (type == null) {
                return null;
            }
            
            PotionEffect effect = new PotionEffect(type, duration > 0 ? duration : 20 * 60, amplifier > -1 ? amplifier : 0, ambient);
            
            return effect;
        }
        
        private PotionEffectType getPotionEffectType() {
            for (PotionEffectType t : PotionEffectType.values()) {
                if (t != null && t.getName() != null) {
                    if (t.getName().equalsIgnoreCase(this.type)) {
                        return t;
                    }
                }
            }
            
            return null;
        }
    }
}
