package co.thecomet.sg.config;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.config.JsonLocation;
import org.bukkit.Bukkit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SGConfig extends JsonConfig {
    public JsonLocation spawn = new JsonLocation(Bukkit.getWorlds().get(0).getSpawnLocation());
    public int minChestPoints = 50;
    public int maxChestPoints = 75;
    public List<ChestItem> chestItems = new ArrayList<>();

    public void save() {
        File file = new File(CoreAPI.getPlugin().getDataFolder(), "config.json");
        this.save(file);
    }
}
