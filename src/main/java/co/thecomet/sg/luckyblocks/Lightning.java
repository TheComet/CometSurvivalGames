package co.thecomet.sg.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Lightning", type = LuckyType.NORMAL, dropChance = 0.025)
public class Lightning extends LuckyBlock {
    @Override
    public boolean canSpawn(Location loc, Player whoBroke) {
        Block above = loc.add(0, 1, 0).getBlock();

        return above.getType() == Material.AIR || above.isLiquid();
    }

    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation();
        loc.getWorld().strikeLightning(player.getLocation());
    }
}
