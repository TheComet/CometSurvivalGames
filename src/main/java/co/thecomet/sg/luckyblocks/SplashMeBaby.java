package co.thecomet.sg.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import org.bukkit.util.Vector;

@LuckyInfo(name = "Splash Me Baby!", type = LuckyType.NORMAL, dropChance = 0.025)
public class SplashMeBaby extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        splash(player, PotionType.STRENGTH);
        splash(player, PotionType.REGEN);
    }
    
    public static void splash(Player player, PotionType type) {
        // Create a potion type
        Potion potion = new Potion(type, 1);

        // Spawn the potion
        ThrownPotion thrown = (ThrownPotion) player.getWorld().spawnEntity(player.getLocation().add(0.0, 3.0, 0.0), EntityType.SPLASH_POTION);
        thrown.setVelocity(new Vector(0.0, -1.0, 0.0));
        thrown.getEffects().addAll(potion.getEffects());
    }
}
