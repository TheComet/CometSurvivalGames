package co.thecomet.sg.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Creeper Eggs", type = LuckyType.NORMAL, dropChance = 0.025)
public class CreeperEggs extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack eggs = ItemBuilder.build(Material.MONSTER_EGG)
                .durability(EntityType.CREEPER.getTypeId())
                .amount(8)
                .build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation(), eggs);
    }
}
