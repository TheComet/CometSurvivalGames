package co.thecomet.sg.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Lucky Boots", type = LuckyType.NORMAL, dropChance = 0.025)
public class LuckyBoots extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack boots = ItemBuilder.build(Material.GOLD_BOOTS)
                .enchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3)
                .enchantment(Enchantment.THORNS, 2)
                .enchantment(Enchantment.PROTECTION_FALL, 2)
                .name(FontColor.rainbow("Lucky Boots", false, false))
                .build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation(), boots);
    }
}
