package co.thecomet.sg.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.*;

@LuckyInfo(name = "Angry Wolf Pack", type = LuckyType.NORMAL, dropChance = 0.025)
public class AngryWolfPack extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation();
        
        for (int i = 0; i < 4; i++) {
            Entity entity = loc.getWorld().spawnEntity(loc, EntityType.WOLF);
            Wolf wolf = (Wolf) entity;
            wolf.setAngry(true);
            wolf.setTarget(player);
        }
    }
}
