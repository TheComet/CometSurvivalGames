package co.thecomet.sg.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.WorldUnloadEvent;

@LuckyInfo(name = "Boobie Trap", type = LuckyType.NORMAL, dropChance = 0.025)
public class BoobieTrap extends LuckyBlock implements Listener {
    private Location loc;

    @Override
    public boolean canSpawn(Location loc, Player whoBroke) {
        Block above = loc.add(0, 1, 0).getBlock();

        return above.getType() == Material.AIR;
    }

    @Override
    public void onBreak(Block block, Player player) {
        this.loc = block.getLocation();
        block.setType(Material.STONE_PLATE);
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.PHYSICAL) {
            if (loc != null) {
                if (event.getClickedBlock().getType() == Material.STONE_PLATE && areLocationsEqual(loc, event.getClickedBlock().getLocation())) {
                    loc.getWorld().createExplosion(loc, 2.0f, false);
                    HandlerList.unregisterAll(this);
                    loc = null;
                }
            }
        }
    }
    
    @EventHandler
    public void onWorldUnload(WorldUnloadEvent event) {
        if (event.getWorld() == loc.getWorld()) {
            HandlerList.unregisterAll(this);
            loc = null;
        }
    }
    
    public boolean areLocationsEqual(Location l1, Location l2) {
        return l1.getBlockX() == l2.getBlockX() && l1.getBlockY() == l2.getBlockY() && l1.getBlockZ() == l2.getBlockZ();
    }
}
