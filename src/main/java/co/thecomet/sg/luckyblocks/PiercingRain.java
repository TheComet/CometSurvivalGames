package co.thecomet.sg.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.UUID;

@LuckyInfo(name = "Piercing Rain", type = LuckyType.NORMAL, dropChance = 0.025)
public class PiercingRain extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        new PiercingRainTask(player).runTaskTimer(CoreAPI.getPlugin(), 1, 4);
    }
    
    public class PiercingRainTask extends BukkitRunnable {
        private UUID uuid;
        private long start;
        
        public PiercingRainTask(Player player) {
            this.uuid = player.getUniqueId();
            this.start = System.currentTimeMillis();
        }
        
        @Override
        public void run() {
            Player player = Bukkit.getPlayer(uuid);
            
            if (player != null && player.getGameMode() != GameMode.SPECTATOR && System.currentTimeMillis() - start < 5 * 1000) {
                for (int i = 0; i < 5; i++) {
                    double positionX = (GeneralUtils.RANDOM.nextBoolean() ? 1.0 : -1.0) * GeneralUtils.RANDOM.nextDouble() * 5.0;
                    double positionZ = (GeneralUtils.RANDOM.nextBoolean() ? 1.0 : -1.0) * GeneralUtils.RANDOM.nextDouble() * 5.0;

                    player.getWorld().spawnArrow(player.getLocation().add(positionX, 3.0, positionZ), new Vector(0.0, -1.0, 0.0), 1.0f, 0.1f);
                }
            } else {
                cancel();
            }
        }
    }
}
