package co.thecomet.sg.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Fishing Rod", type = LuckyType.NORMAL, dropChance = 0.025)
public class FishingRod extends LuckyBlock {
    private static ItemStack rod = ItemBuilder.build(Material.FISHING_ROD).build();
    
    @Override
    public void onBreak(Block block, Player player) {
        player.getWorld().dropItem(player.getLocation(), rod);
    }
}
