package co.thecomet.sg.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Lucky Bow", type = LuckyType.NORMAL, dropChance = 0.025)
public class LuckyBow extends LuckyBlock implements Listener {
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack bow = ItemBuilder.build(Material.BOW)
                .enchantment(Enchantment.ARROW_DAMAGE, 3)
                .name(FontColor.rainbow("Lucky Bow", false, false))
                .build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation(), bow);
    }

    @EventHandler
    public void onProjectileHit(ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            Player player = (Player) event.getEntity().getShooter();
            if (player.getItemInHand() != null && player.getItemInHand().getType() == Material.BOW) {
                if (player.getItemInHand().hasItemMeta()) {
                    if (FontColor.stripColor(player.getItemInHand().getItemMeta().getDisplayName()).equalsIgnoreCase("Lucky Axe")) {
                        event.getEntity().setCustomName("Lucky Arrow");
                    }
                }
            }
        }
    }
    
    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        if (event.getEntity().getCustomName() != null) {
            if (event.getEntity().getCustomName().equalsIgnoreCase("Lucky Arrow")) {
                Location location = event.getEntity().getLocation();
                World world = location.getWorld();
                world.createExplosion(location, 1.0f, false);
                event.getEntity().remove();
            }
        }
    }

    @EventHandler
    public void onEntityExplodeEvent(EntityExplodeEvent event) {
        event.setCancelled(true);
    }
}
