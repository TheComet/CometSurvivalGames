package co.thecomet.sg.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import com.google.common.collect.Lists;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.List;

@LuckyInfo(name = "Foodie", type = LuckyType.NORMAL, dropChance = 0.1)
public class Foodie extends LuckyBlock {
    private static List<Material> drops = Lists.newArrayList(Material.COOKED_CHICKEN, Material.COOKED_BEEF,
            Material.COOKED_FISH, Material.COOKED_MUTTON,
            Material.COOKED_RABBIT, Material.CARROT_ITEM,
            Material.BAKED_POTATO, Material.MUSHROOM_SOUP,
            Material.APPLE, Material.COOKIE, Material.POTATO_ITEM);
    
    @Override
    public void onBreak(Block block, Player player) {
        Collections.shuffle(drops, GeneralUtils.RANDOM);

        Material mat = drops.get(GeneralUtils.getBetween(0, drops.size()));
        ItemStack drop = ItemBuilder.build(mat).amount(GeneralUtils.getBetween(1, 5)).build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(loc, drop);
    }
}
