package co.thecomet.sg.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@LuckyInfo(name = "Lucky Sword", type = LuckyType.NORMAL, dropChance = 0.025)
public class LuckySword extends LuckyBlock implements Listener {
    private static Cache<UUID, Long> cooldown = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();

    @Override
    public void onBreak(Block block, Player player) {
        ItemStack sword = ItemBuilder.build(Material.GOLD_SWORD)
                .enchantment(Enchantment.DAMAGE_ALL, 2)
                .enchantment(Enchantment.DURABILITY, 2)
                .name(FontColor.rainbow("Lucky Sword", false, false))
                .build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation(), sword);
    }

    @EventHandler
    public void onPlayerAttack(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) {
            return;
        }
        
        if (((Player) event.getDamager()).getItemInHand() == null) {
            return;
        }
        
        ItemStack item = ((Player) event.getDamager()).getItemInHand();
        if (item.hasItemMeta() == false || item.getItemMeta().hasDisplayName() == false) {
            return;
        }
        
        if (ChatColor.stripColor(item.getItemMeta().getDisplayName()).equals("Lucky Sword")) {
            if (cooldown.asMap().containsKey(event.getDamager().getUniqueId()) == false) {
                event.getDamager().getWorld().strikeLightning(event.getEntity().getLocation());
                cooldown.put(event.getDamager().getUniqueId(), System.currentTimeMillis());
            }
        }
    }
}
