package co.thecomet.sg.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

@LuckyInfo(name = "Chicky", type = LuckyType.NORMAL, dropChance = 0.025)
public class Chicky extends LuckyBlock {
    private static ItemStack diamond = ItemBuilder.build(Material.DIAMOND).build();

    @Override
    public void onBreak(Block block, Player player) {
        new ChickyTask(block).runTaskTimer(CoreAPI.getPlugin(), GeneralUtils.getBetween(20 * 5, 20 * 15), GeneralUtils.getBetween(20 * 5, 20 * 15));
    }

    public class ChickyTask extends BukkitRunnable {
        private UUID world;
        private UUID entity;
        private int count = 0;
        
        public ChickyTask(Block block) {
            Location loc = block.getLocation();
            Chicken chicken = (Chicken) loc.getWorld().spawnEntity(loc, EntityType.CHICKEN);
            chicken.setCustomNameVisible(true);
            chicken.setCustomName("Chicky");
            
            this.world = loc.getWorld().getUID();
            this.entity = chicken.getUniqueId();
        }
        
        @Override
        public void run() {
            World w = Bukkit.getWorld(world);
            Chicken chicken = null;
            
            if (w != null) {
                for (Chicken c : w.getEntitiesByClass(Chicken.class)) {
                    if (c.getUniqueId() == entity) {
                        chicken = c;
                    }
                }
            }
            
            if (w != null && chicken != null && count < 5) {
                w.dropItem(chicken.getLocation(), diamond);
                count++;
            } else {
                cancel();
            }
        }
    }
}
