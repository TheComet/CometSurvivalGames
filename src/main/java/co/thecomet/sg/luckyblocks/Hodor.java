package co.thecomet.sg.luckyblocks;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Hodor", type = LuckyType.NORMAL, dropChance = 0.025)
public class Hodor extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation();
        
        Entity entity = loc.getWorld().spawnEntity(loc, EntityType.VILLAGER);
        entity.setCustomName("Hodor");
        entity.setCustomNameVisible(true);
        Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage("Hodor: HODOR!!!"));
        
        Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
            entity.remove();
            loc.getWorld().createExplosion(loc, 2.0f, false);
        }, 20 * 2);
    }
}
