package co.thecomet.sg.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Lucky Chestplate", type = LuckyType.NORMAL, dropChance = 0.025)
public class LuckyChestplate extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack chestplate = ItemBuilder.build(Material.GOLD_CHESTPLATE)
                .enchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3)
                .enchantment(Enchantment.THORNS, 2)
                .name(FontColor.rainbow("Lucky Chestplate", false, false))
                .build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation(), chestplate);
    }
}
