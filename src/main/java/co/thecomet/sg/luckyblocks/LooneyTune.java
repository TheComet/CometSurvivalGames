package co.thecomet.sg.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Looney Tune", type = LuckyType.NORMAL, dropChance = 0.025)
public class LooneyTune extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        for (int x = -5; x <= 5; x++) {
            for (int z = -5; z <= 5; z++) {
                Location location = player.getLocation().add(x, 5, z);
                if (location.getBlock() != null && location.getBlock().getType() == Material.AIR) {
                    if (GeneralUtils.RANDOM.nextDouble() < 0.25) {
                        location.getBlock().setType(Material.ANVIL);
                    }
                }
            }
        }
    }
}
