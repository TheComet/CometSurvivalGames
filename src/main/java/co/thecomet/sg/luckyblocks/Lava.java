package co.thecomet.sg.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Lava", type = LuckyType.NORMAL, dropChance = 0.025)
public class Lava extends LuckyBlock {
   @Override
    public void onBreak(Block block, Player player) {
        block.setType(Material.LAVA);
    }
}
