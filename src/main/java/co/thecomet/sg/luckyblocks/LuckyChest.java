package co.thecomet.sg.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@LuckyInfo(name = "Lucky Chest", type = LuckyType.NORMAL, dropChance = 0.1)
public class LuckyChest extends LuckyBlock {
    private static List<Material> LOOT = new ArrayList<>();
    
    static {
        LOOT.add(Material.IRON_SWORD);
        LOOT.add(Material.STONE_SWORD);
        LOOT.add(Material.LEATHER_CHESTPLATE);
        LOOT.add(Material.LEATHER_BOOTS);
        LOOT.add(Material.LEATHER_LEGGINGS);
        LOOT.add(Material.BOW);
        LOOT.add(Material.ARROW);
        LOOT.add(Material.CHAINMAIL_HELMET);
        LOOT.add(Material.CHAINMAIL_CHESTPLATE);
        LOOT.add(Material.WOOD_SWORD);
        LOOT.add(Material.STONE_AXE);
        LOOT.add(Material.IRON_LEGGINGS);
        LOOT.add(Material.IRON_BOOTS);
        LOOT.add(Material.GOLD_AXE);
        LOOT.add(Material.GOLD_HELMET);
        LOOT.add(Material.GOLD_SWORD);
    }
    
    @Override
    public boolean canSpawn(Location loc, Player whoBroke) {
        Block above = loc.add(0, 1, 0).getBlock();

        return above.getType() == Material.AIR || above.isLiquid();
    }

    @Override
    public void onBreak(Block block, Player player) {
        Collections.shuffle(LOOT);

        block.getLocation().getBlock().setType(Material.CHEST);
        Chest chest = (Chest) block.getState();
        Inventory inv = chest.getBlockInventory();

        List<ItemStack> loot = new ArrayList<>();
        for (int i = 0; loot.size() < 2; i++) {
            Material next = LOOT.get(GeneralUtils.getBetween(0, LOOT.size() - 1));

            boolean exists = false;
            for (ItemStack stack : loot) {
                if (stack.getType() == next) {
                    exists = true;
                }
            }
            
            if (exists == false) {
                loot.add(ItemBuilder.build(next).amount(next == Material.ARROW ? GeneralUtils.getBetween(1, 5) : 1).build());
            }
        }
        
        for (ItemStack item : loot) {
            int slot = GeneralUtils.getBetween(0, 26);
            
            while (inv.getItem(slot) != null) {
                slot = GeneralUtils.getBetween(0, 26);
            }
            
            inv.setItem(slot, item);
        }
    }
}
