package co.thecomet.sg.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@LuckyInfo(name = "Lucky Axe", type = LuckyType.NORMAL, dropChance = 0.025)
public class LuckyAxe extends LuckyBlock implements Listener {
    private static Cache<UUID, Long> cooldown = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();

    @Override
    public void onBreak(Block block, Player player) {
        ItemStack axe = ItemBuilder.build(Material.GOLD_AXE)
                .enchantment(Enchantment.DAMAGE_ALL, 2)
                .enchantment(Enchantment.DURABILITY, 2)
                .name(FontColor.rainbow("Lucky Axe", false, false))
                .build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation(), axe);
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getItem() == null) {
            return;
        }
        
        if (!event.getAction().equals(Action.RIGHT_CLICK_AIR) && !event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }
        
        if (event.getItem().getItemMeta() == null || event.getItem().getItemMeta().getDisplayName() == null || event.getItem().getItemMeta().getDisplayName().isEmpty()) {
            return;
        }
        
        if (ChatColor.stripColor(event.getItem().getItemMeta().getDisplayName()).equals("Lucky Axe")) {
            if (cooldown.asMap().containsKey(event.getPlayer().getUniqueId()) == false) {
                event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 3, 1));
                cooldown.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
            }
        }
    }
}
