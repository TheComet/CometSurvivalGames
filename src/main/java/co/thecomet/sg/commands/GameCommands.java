package co.thecomet.sg.commands;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.minigameapi.world.MapLoadException;
import co.thecomet.minigameapi.world.MapManager;
import co.thecomet.sg.game.GameLoop;
import co.thecomet.sg.config.SGMapConfig;
import co.thecomet.sg.game.phases.PreGamePhase;
import co.thecomet.sg.game.phases.StartingPhase;
import org.bukkit.entity.Player;

public class GameCommands {
    public GameCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "forcestart", Rank.ADMIN, GameCommands::forceStart);
    }
    
    public static void forceStart(Player player, String[] args) {
        if (args.length > 2) {
            MessageFormatter.sendUsageMessage(player, "/forcestart [-t] [-m]");
            return;
        }
        
        boolean testing = false;
        boolean senderVotedMap = false;
        if (args.length > 0) {
            for (String flag : args) {
                if (flag.equalsIgnoreCase("-t")) {
                    testing = true;
                }
                
                if (flag.equalsIgnoreCase("-m")) {
                    senderVotedMap = true;
                }
            }
        }
        
        GameLoop gameLoop = GameLoop.getInstance();
        if (gameLoop.getPhase() instanceof PreGamePhase) {
            MapManager<SGMapConfig> mapManager = gameLoop.getMapManager();
            if (mapManager.getMaps().isEmpty()) {
                MessageFormatter.sendErrorMessage(player, "There are no configured maps!");
                return;
            }
            
            LoadedMap<SGMapConfig> map = ((PreGamePhase) gameLoop.getPhase()).getWinningMap();
            if (senderVotedMap && ((PreGamePhase) gameLoop.getPhase()).getMapMenu().hasVoted(player)) {
                map = ((PreGamePhase) gameLoop.getPhase()).getMapMenu().getVotedMap(player);
            }
            mapManager.setCurrent(map);
            
            mapManager.setUpdating(false);
            try {
                mapManager.loadSelectedMap(map);
            } catch (MapLoadException e) {
                mapManager.setUpdating(true);
                gameLoop.setPhase(new PreGamePhase(true));
                return;
            }

            gameLoop.setPhase(new StartingPhase(testing));
        } else {
            MessageFormatter.sendErrorMessage(player, "You can only force start during pregame phase.");
            return;
        }
    }
}
