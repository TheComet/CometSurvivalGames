package co.thecomet.sg.commands;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.config.JsonConfig;
import co.thecomet.common.user.Rank;
import co.thecomet.common.utils.world.generator.VoidGenerator;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.config.JsonLocation;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.minigameapi.world.MapManager;
import co.thecomet.sg.SurvivalGames;
import co.thecomet.sg.config.ChestItem;
import co.thecomet.sg.config.SGConfig;
import co.thecomet.sg.game.GameLoop;
import co.thecomet.sg.config.SGMapConfig;
import co.thecomet.sg.game.phases.PreGamePhase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaintenanceCommands {
    private static boolean operationInProgress = false;
    
    public MaintenanceCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "setspawn", Rank.ADMIN, MaintenanceCommands::setSpawn);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "mm", Rank.ADMIN, MaintenanceCommands::maintenance);
    }
    
    public static void setSpawn(Player player, String[] args) {
        if (player.getWorld() != Bukkit.getWorlds().get(0)) {
            MessageFormatter.sendErrorMessage(player, "You must be in the main world!");
            return;
        }
        
        SGConfig config = SurvivalGames.getSGConfig();
        config.spawn = new JsonLocation(player);
        config.save();
        MessageFormatter.sendSuccessMessage(player, "You have set the spawn to your location.");
    }

    /**
     * Command that enables/disables maintenance mode and
     * handles all necessary command registration.
     * *
     * @param sender
     * @param args
     */
    public static void maintenance(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/mm <true|false>");
            return;
        }
        
        Boolean val;
        try {
            val = Boolean.parseBoolean(args[0]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/mm <true|false>");
            return;
        }
        
        if (val.booleanValue()) {
            if (GameLoop.getInstance().isMaintenanceEnabled()) {
                MessageFormatter.sendErrorMessage(sender, "Maintenance mode is already enabled!");
            } else {
                if (GameLoop.getInstance().getPhase() instanceof PreGamePhase == false) {
                    MessageFormatter.sendErrorMessage(sender, "Maintenance can only be enabled during pregame phase!");
                    return;
                }
                
                GameLoop.getInstance().setMaintenanceEnabled(true);
                CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tpw", Rank.ADMIN, MaintenanceCommands::tpw);
                CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "conf", Rank.ADMIN, MaintenanceCommands::conf);
                CommandRegistry.registerPlayerSubCommand("conf", "setmapname", Rank.ADMIN, MaintenanceCommands::setMapName);
                CommandRegistry.registerPlayerSubCommand("conf", "setauthorname", Rank.ADMIN, MaintenanceCommands::setAuthorName);
                CommandRegistry.registerPlayerSubCommand("conf", "setspectatorspawn", Rank.ADMIN, MaintenanceCommands::setSpectatorSpawn);
                CommandRegistry.registerPlayerSubCommand("conf", "setarenacenter", Rank.ADMIN, MaintenanceCommands::setArenaCenter);
                CommandRegistry.registerPlayerSubCommand("conf", "addpedestal", Rank.ADMIN, MaintenanceCommands::addPedestal);
                CommandRegistry.registerPlayerSubCommand("conf", "clearpedestals", Rank.ADMIN, MaintenanceCommands::clearPedestals);
                CommandRegistry.registerPlayerSubCommand("conf", "tpvalidatepedestals", Rank.ADMIN, MaintenanceCommands::tpValidatePedestals);
                CommandRegistry.registerPlayerSubCommand("conf", "detectchests", Rank.ADMIN, MaintenanceCommands::detectChests);
                CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "map", Rank.ADMIN, MaintenanceCommands::map);
                CommandRegistry.registerPlayerSubCommand("map", "makesafe", Rank.ADMIN, MaintenanceCommands::makeSafe);
                CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "chests", Rank.ADMIN, MaintenanceCommands::chests);
                CommandRegistry.registerPlayerSubCommand("chests", "add", Rank.ADMIN, MaintenanceCommands::add);
                CommandRegistry.registerPlayerSubCommand("chests", "enchant", Rank.ADMIN, MaintenanceCommands::enchant);


                for (Player player : Bukkit.getOnlinePlayers()) {
                    NetworkPlayer bp = CoreAPI.getPlayer(player);
                    if (bp == null || bp.getRank().ordinal() < Rank.MOD.ordinal()) {
                        player.kickPlayer("\nMaintenance mode has been enabled.\nWe are sorry for the inconvenience.");
                    } else {
                        player.setGameMode(GameMode.CREATIVE);
                        player.getInventory().clear();
                        player.updateInventory();
                    }
                }
                
                GameLoop.getInstance().teleportAllToLobby();
            }
        } else {
            if (GameLoop.getInstance().isMaintenanceEnabled()) {
                GameLoop.getInstance().setMaintenanceEnabled(false);
                CommandRegistry.unregisterCommand("tpw");
                CommandRegistry.unregisterCommand("conf");
                CommandRegistry.unregisterCommand("map");
                CommandRegistry.unregisterCommand("chests");
            } else {
                MessageFormatter.sendErrorMessage(sender, "Maintenance mode is not enabled!");
            }
        }
    }

    /**
     * Command to teleport a player to another world.
     * * 
     * @param sender
     * @param args
     */
    public static void tpw(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/tpw <world>");
            return;
        }
        
        if (Bukkit.getWorld(args[0]) != null) {
            if (Bukkit.getWorlds().get(0).equals(Bukkit.getWorld(args[0]))) {
                sender.teleport(SurvivalGames.getSGConfig().spawn.getLocation());
                return;
            }
        }

        MapManager<SGMapConfig> manager = GameLoop.getInstance().getMapManager();
        String world = args[0];
        for (File dir : manager.getAllWorlds().values()) {
            if (dir.getName().equalsIgnoreCase(Bukkit.getWorlds().get(0).getName())) {
                continue;
            }

            if (dir.getName().equalsIgnoreCase(world)) {
                SGMapConfig config = JsonConfig.load(new File(dir, "map.json"), SGMapConfig.class);
                config.save(dir);
            } else {
                continue;
            }
            
            World destination = null;
            for (World w : Bukkit.getWorlds()) {
                if (w.getName().equalsIgnoreCase(world)) {
                    destination = w;
                }
            }
            
            if (destination == null) {
                File destDir = new File(Bukkit.getWorldContainer(), dir.getName());
                try {
                    if (destDir.exists()) {
                        FileUtils.deleteDirectory(destDir);
                    }

                    FileUtils.copyDirectory(dir, destDir);
                } catch (IOException e) {
                    MessageFormatter.sendErrorMessage(sender, "Failed to copy the file from the maps folder.");
                    return;
                }
                
                for (File file : destDir.listFiles()) {
                    if (file.getName().equalsIgnoreCase("uid.dat") || file.getName().equalsIgnoreCase("session.lock")) {
                        file.delete();
                    }
                }
                
                destination = new WorldCreator(dir.getName())
                        .generateStructures(false)
                        .environment(World.Environment.NORMAL)
                        .type(WorldType.NORMAL)
                        .generator(new VoidGenerator())
                        .createWorld();
                destination.setAutoSave(false);
                
                for (Entity entity : destination.getEntities()) {
                    if ((entity instanceof Player) == false) {
                        entity.remove();
                    }
                }
            }
            
            sender.teleport(destination.getSpawnLocation());
            MessageFormatter.sendSuccessMessage(sender, "You have been teleported to &6" + destination.getName());
            return;
        }
        
        MessageFormatter.sendErrorMessage(sender, "That world does not exists!");
    }

    /**
     * Main command for map configuration commands.
     * *
     * @param player
     * @param args
     */
    public static void conf(Player player, String[] args) {
        MessageFormatter.sendUsageMessage(player, "/conf setmapname <name>");
        MessageFormatter.sendUsageMessage(player, "/conf setauthorname <name>");
        MessageFormatter.sendUsageMessage(player, "/conf setspectatorspawn");
        MessageFormatter.sendUsageMessage(player, "/conf setarenacenter");
        MessageFormatter.sendUsageMessage(player, "/conf addpedestal");
        MessageFormatter.sendUsageMessage(player, "/conf clearpedestals");
        MessageFormatter.sendUsageMessage(player, "/conf tpvalidatepedestals");
        MessageFormatter.sendUsageMessage(player, "/conf detectchests <radius>");
    }
    
    public static void setMapName(Player player, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(player, "/conf setmapname <name>");
            return;
        }
        
        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }
        
        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }
        
        map.getConfig().mapName = StringUtils.join(args, " ");
        map.getConfig().save(new File(map.getWorld(), "map.json"));
        
        MessageFormatter.sendSuccessMessage(player, "You have set the map name to &6" + map.getMapName());
    }
    
    public static void setAuthorName(Player player, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(player, "/conf setauthorname <name>");
            return;
        }

        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().mapAuthor = StringUtils.join(args, " ");
        map.getConfig().save(new File(map.getWorld(), "map.json"));

        MessageFormatter.sendSuccessMessage(player, "You have set the map author to &6" + map.getMapAuthor());
    }
    
    public static void addPedestal(Player player, String[] args) {
        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().pedestals.add(new JsonLocation(player));
        map.getConfig().save(new File(map.getWorld(), "map.json"));
        
        MessageFormatter.sendSuccessMessage(player, "Pedestal set at your position!");
    }

    public static void setSpectatorSpawn(Player player, String[] args) {
        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().spectatorSpawn = new JsonLocation(player);
        map.getConfig().save(new File(map.getWorld(), "map.json"));

        MessageFormatter.sendSuccessMessage(player, "Spectator spawn set to your position!");
    }

    public static void setArenaCenter(Player player, String[] args) {
        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        map.getConfig().arenaCenter = new JsonLocation(player);
        map.getConfig().save(new File(map.getWorld(), "map.json"));

        MessageFormatter.sendSuccessMessage(player, "Arena center set to your position!");
    }
    
    public static void clearPedestals(Player player, String[] args) {
        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }
        
        map.getConfig().pedestals = new ArrayList<>();
        map.getConfig().save(new File(map.getWorld(), "map.json"));
        
        MessageFormatter.sendSuccessMessage(player, "Pedestals have been cleared!");
    }
    
    public static void tpValidatePedestals(Player player, String[] args) {
        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }
        
        if (map.getConfig().pedestals.size() == 0) {
            MessageFormatter.sendErrorMessage(player, "This map has no pedestals configured!");
            return;
        }

        
        for (int i = 0; i < map.getConfig().pedestals.size(); i++) {
            JsonLocation location = map.getConfig().pedestals.get(i);
            Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> player.teleport(location.getLocation()), 20 * (1 + i));
        }
        
        Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> MessageFormatter.sendSuccessMessage(player, "You have tp validated all pedestals!"), 20 * map.getConfig().pedestals.size());
    }
    
    public static void detectChests(Player player, String[] args) {
        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }
        
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/conf detectchests <radius>");
            return;
        }
        
        int radius;
        try {
            radius = Math.abs(Integer.parseInt(args[0]));
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(player, "/conf detectchests <radius>");
            return;
        }
        
        if (operationInProgress) {
            MessageFormatter.sendErrorMessage(player, "There is already an operation in progress. Please wait a bit.");
            return;
        }

        operationInProgress = true;
        final List<JsonLocation> chests = new ArrayList<>();
        Chunk chunk = player.getLocation().getChunk();
        int next = 0;
        for (int x = chunk.getX() - radius; x < chunk.getX() + radius; x++) {
            for (int z = chunk.getZ() - radius; z < chunk.getZ() + radius; z++) {
                final int fx = x;
                final int fz = z;
                Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
                    Chunk c = chunk.getWorld().getChunkAt(fx, fz);
                    boolean loaded = c.isLoaded() == true ? true : c.load(false);

                    if (loaded) {
                        for (int bx = 0; bx < 16; bx++) {
                            for (int bz = 0; bz < 16; bz++) {
                                for (int by = 0; by < c.getWorld().getMaxHeight(); by++) {
                                    Block block = c.getWorld().getChunkAt(fx, fz).getBlock(bx, by, bz);
                                    if (block.getType() == Material.CHEST) {
                                        chests.add(new JsonLocation(block));
                                    }
                                }
                            }
                        }
                    }
                    
                    c.unload(false, true);
                }, next * 5);
            }
            
            next++;
        }
        
        final LoadedMap<SGMapConfig> fmap = map;
        Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
            fmap.getConfig().chests = chests;
            fmap.getConfig().save(new File(fmap.getWorld(), "map.json"));
            MessageFormatter.sendSuccessMessage(player, FontColor.ORANGE + String.valueOf(chests.size()) + " chests &afound and registered!");
            operationInProgress = false;
        }, next * 5);
    }
    
    public static void map(Player player, String[] args) {
        MessageFormatter.sendUsageMessage(player, "/map makesafe <radius>");
    }

    public static void makeSafe(Player player, String[] args) {
        LoadedMap<SGMapConfig> map = null;
        for (LoadedMap m : GameLoop.getInstance().getMapManager().getMaps()) {
            if (m.getWorld().getName().equalsIgnoreCase(player.getWorld().getName())) {
                map = m;
            }
        }

        if (map == null) {
            MessageFormatter.sendErrorMessage(player, "The map you are in is not configurable at this time.");
            return;
        }

        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/map makesafe <radius>");
            return;
        }

        int radius;
        try {
            radius = Math.abs(Integer.parseInt(args[0]));
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(player, "/map makesafe <radius>");
            return;
        }

        if (operationInProgress) {
            MessageFormatter.sendErrorMessage(player, "There is already an operation in progress. Please wait a bit.");
            return;
        }

        operationInProgress = true;
        final List<Long> count = new ArrayList<>();
        Chunk chunk = player.getLocation().getChunk();
        int next = 0;
        for (int x = chunk.getX() - radius; x < chunk.getX() + radius; x++) {
            for (int z = chunk.getZ() - radius; z < chunk.getZ() + radius; z++) {
                final int fx = x;
                final int fz = z;
                Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
                    Chunk c = chunk.getWorld().getChunkAt(fx, fz);
                    boolean loaded = c.isLoaded() == true ? true : c.load(false);

                    if (loaded) {
                        for (int bx = 0; bx < 16; bx++) {
                            for (int bz = 0; bz < 16; bz++) {
                                for (int by = 0; by < c.getWorld().getMaxHeight(); by++) {
                                    Block block = c.getWorld().getChunkAt(fx, fz).getBlock(bx, by, bz);
                                    if (block.getType() == Material.COMMAND || block.getType() == Material.COMMAND_MINECART) {
                                        block.setType(Material.AIR);
                                        count.add(System.currentTimeMillis());
                                    }
                                }
                            }
                        }
                    }

                    c.unload(false, true);
                }, next * 5);
            }

            next++;
        }

        Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
            MessageFormatter.sendSuccessMessage(player, FontColor.ORANGE + String.valueOf(count.size() + " command blocks were removed!"));
            operationInProgress = false;
        }, next * 5);
    }

    /**
     * Main command for ChestItem configuration.
     * *
     * @param player
     * @param args
     */
    public static void chests(Player player, String[] args) {
        MessageFormatter.sendUsageMessage(player, "/chests add [-min min] [-max max] [-p requiredChestPoints] [-r rarity]");
        MessageFormatter.sendUsageMessage(player, "/chests enchant <enchantment> [level]");
    }
    
    public static void add(Player player, String[] args) {
        ItemStack itemStack = player.getItemInHand();
        if (itemStack == null || itemStack.getType() == Material.AIR) {
            MessageFormatter.sendErrorMessage(player, "You must be holding the item that you want to add in your hand.");
            return;
        }

        ChestItem chestItem = null;
        int min = 1;
        int max = 1;
        int requiredChestPoints = 5;
        double rarity = 1.0;
        if (args.length >= 2) {
            for (int i = 0; i < args.length; i += 2) {
                try {
                    if (i < args.length - 1) {
                        switch (args[i]) {
                            case "-min":
                                min = Integer.parseInt(args[i + 1]);
                                break;
                            case "-max":
                                max = Integer.parseInt(args[i + 1]);
                                break;
                            case "-p":
                                requiredChestPoints = Integer.parseInt(args[i + 1]);
                                break;
                            case "-r":
                                double num = Double.parseDouble(args[i + 1]);
                                rarity = num > 1.0 ? 1.0 : num <= 0.0 ? 0.01 : num;
                                break;
                            default:
                                continue;
                        }
                    }
                } catch (NumberFormatException e) {
                    MessageFormatter.sendUsageMessage(player, "/chests add [-min min] [-max max] [-p requiredChestPoints] [-r rarity]");
                    break;
                }
            }

            if (max < min) {
                max = min;
            }

            chestItem = new ChestItem(itemStack, min, max, requiredChestPoints, rarity);
        } else {
            chestItem = new ChestItem(itemStack);
        }

        SGConfig config = SurvivalGames.getSGConfig();
        if (config.chestItems == null) {
            config.chestItems = new ArrayList<>();
        }
        config.chestItems.add(chestItem);
        config.save();
        
        MessageFormatter.sendSuccessMessage(player, "The item was saved to the config!");
    }
    
    public static void enchant(Player player, String[] args) {
        ItemStack itemStack = player.getItemInHand();
        if (itemStack == null || itemStack.getType() == Material.AIR) {
            MessageFormatter.sendErrorMessage(player, "You must be holding the item that you want to add in your hand.");
            return;
        }
        
        if (args.length > 2 || args.length < 1) {
            MessageFormatter.sendUsageMessage(player, "/chests enchant <enchantment> [level]");
            return;
        }

        Enchantment enchantment = Enchantment.getByName(args[0].toUpperCase());
        int level = 1;
        if (args.length == 2) {
            try {
                level = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                MessageFormatter.sendUsageMessage(player, "/chests enchant <enchantment> [level]");
                return;
            }
        }
        
        if (enchantment == null) {
            List<String> enchantments = new ArrayList<>();
            Arrays.asList(Enchantment.values()).forEach(e -> enchantments.add(e.getName()));
            MessageFormatter.sendInfoMessage(player, "&7Enchants&8: &6" + StringUtils.join(enchantments, ", "));
            return;
        }
        
        itemStack.addUnsafeEnchantment(enchantment, level);
        MessageFormatter.sendSuccessMessage(player, "The enchantment has been added to the item you are holding!");
    }
}
