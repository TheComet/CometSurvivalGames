package co.thecomet.sg.commands;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.sg.game.GameLoop;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import java.io.File;

public class DebugCommands {
    public DebugCommands() {
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "debug", Rank.ADMIN, DebugCommands::debug);
        CommandRegistry.registerUniversalSubCommand("debug", "ulist", Rank.ADMIN, DebugCommands::ulist);
        CommandRegistry.registerUniversalSubCommand("debug", "mlist", Rank.ADMIN, DebugCommands::mlist);
        CommandRegistry.registerUniversalSubCommand("debug", "llist", Rank.ADMIN, DebugCommands::llist);
        CommandRegistry.registerUniversalSubCommand("debug", "alist", Rank.ADMIN, DebugCommands::alist);
        CommandRegistry.registerUniversalSubCommand("debug", "pinfo", Rank.ADMIN, DebugCommands::pinfo);
    }
    
    public static void debug(CommandSender sender, String[] args) {
        MessageFormatter.sendUsageMessage(sender, "/debug ulist");
        MessageFormatter.sendUsageMessage(sender, "/debug mlist");
        MessageFormatter.sendUsageMessage(sender, "/debug llist");
        MessageFormatter.sendUsageMessage(sender, "/debug alist");
        MessageFormatter.sendUsageMessage(sender, "/debug pinfo");
    }
    
    public static void ulist(CommandSender sender, String[] args) {
        for (File f : GameLoop.getInstance().getMapManager().getUnloadedWorlds()) {
            MessageFormatter.sendInfoMessage(sender, "Directory: " + f.getName());
        }
    }
    
    public static void mlist(CommandSender sender, String[] args) {
        for (LoadedMap map : GameLoop.getInstance().getMapManager().getMaps()) {
            MessageFormatter.sendInfoMessage(sender, "Map: " + map.getMapName() + " Author: " + map.getMapAuthor());
        }
    }
    
    public static void llist(CommandSender sender, String[] args) {
        for (World world : Bukkit.getWorlds()) {
            MessageFormatter.sendInfoMessage(sender, "World: " + world.getName());
        }
    }

    public static void alist(CommandSender sender, String[] args) {
        for (File f : GameLoop.getInstance().getMapManager().getAllWorlds().values()) {
            MessageFormatter.sendInfoMessage(sender, "Directory: " + f.getName());
        }
    }
    
    public static void pinfo(CommandSender sender, String[] args) {
        MessageFormatter.sendInfoMessage(sender, "Phase: " + GameLoop.getInstance().getPhase().getClass().getSimpleName());
        for (String info : GameLoop.getInstance().getPhase().getPhaseInfo()) {
            MessageFormatter.sendGeneralMessage(sender, info);
        }
    }
}
