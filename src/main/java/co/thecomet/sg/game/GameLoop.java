package co.thecomet.sg.game;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.common.utils.world.generator.VoidGenerator;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.WorldCleaner;
import co.thecomet.minigameapi.world.MapManager;
import co.thecomet.minigameapi.world.PostMapLoadEvent;
import co.thecomet.minigameapi.world.PreMapLoadEvent;
import co.thecomet.sg.SurvivalGames;
import co.thecomet.sg.commands.DebugCommands;
import co.thecomet.sg.commands.GameCommands;
import co.thecomet.sg.commands.MaintenanceCommands;
import co.thecomet.sg.game.classes.GameClass;
import co.thecomet.sg.game.classes.Tribute;
import co.thecomet.sg.config.SGMapConfig;
import co.thecomet.sg.game.phases.*;
import co.thecomet.sg.game.player.Participant;
import co.thecomet.sg.luckyblocks.*;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.potion.PotionEffect;

import java.util.*;
import java.util.stream.Collectors;

public class GameLoop implements Listener, Runnable {
    private static GameLoop instance;
    private SurvivalGames plugin;
    private final MapManager<SGMapConfig> mapManager;
    private Phase phase;
    private GameScoreboardManager gameScoreboardManager;
    private boolean maintenanceEnabled = false;
    private Map<UUID, Participant> participants = new HashMap<>();
    private Map<UUID, PermissionAttachment> permissions = new HashMap<>();
    private Map<Class<? extends GameClass>, GameClass> gameClasses = new HashMap<>();
    private List<Listener> gameClassListeners = new ArrayList<>();
    
    public GameLoop(SurvivalGames plugin) {
        instance = this;
        this.plugin = plugin;
        this.mapManager = new MapManager<>(plugin, SGMapConfig.class);
        this.phase = new PreGamePhase(false);
        this.gameScoreboardManager = new GameScoreboardManager(this);

        Bukkit.getPluginManager().registerEvents(this, plugin);
        new DebugCommands();
        new MaintenanceCommands();
        new GameCommands();

        Bukkit.getWorlds().get(0).getEntities().stream().filter(entity -> entity instanceof Player == false).forEach(org.bukkit.entity.Entity::remove);
        
        CoreAPI.getLuckyBlocks().setCanCancel(true);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(AngryWolfPack.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(BoobieTrap.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(Bobby.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(Chicky.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(CreeperEggs.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(DecoyChest.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(Enderpearls.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(FishingRod.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(Foodie.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(Hodor.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(IronSword.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(Lava.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(Lightning.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(LooneyTune.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(LuckyAxe.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(LuckyBoots.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(LuckyBow.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(LuckyChest.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(LuckyChestplate.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(LuckySword.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(PiercingRain.class);
        CoreAPI.getLuckyBlocks().registerLuckyBlock(SplashMeBaby.class);
    }

    @Override
    public void run() {
        if (maintenanceEnabled) {
            if (phase instanceof MaintenancePhase == false) {
                setPhase(new MaintenancePhase());
            }
        } else {
            if (phase instanceof MaintenancePhase) {
                setPhase(phase.next());
            }
        }
        
        if (phase.hasCompleted()) {
            setPhase(phase.next());
        }
        
        phase.tick();
    }
    
    public MapManager<SGMapConfig> getMapManager() {
        return mapManager;
    }
    
    public List<Participant> getParticipants() {
        return new ArrayList<>(participants.values());
    }
    
    public Participant getParticipant(UUID uuid) {
        return participants.get(uuid);
    }

    public Phase getPhase() {
        return phase;
    }

    public void setPhase(Phase phase) {
        if (this.phase != null) {
            Phase old = this.phase;
            
            if (old instanceof Listener) {
                HandlerList.unregisterAll((Listener) old);
            }

            old.cleanup();
        }
        
        this.phase = phase;

        if (phase instanceof InProgressPhase) {
            gameClassListeners.addAll(new ArrayList<>(gameClasses.values()).stream()
                    .filter(gameClass -> gameClass instanceof Listener)
                    .map(gameClass -> (Listener) gameClass)
                    .collect(Collectors.toList()));

            for (Listener listener : gameClassListeners) {
                Bukkit.getPluginManager().registerEvents(listener, CoreAPI.getPlugin());
            }
        } else {
            gameClassListeners.forEach(HandlerList::unregisterAll);
            
            if (phase instanceof PreGamePhase || phase instanceof MaintenancePhase) {
                mapManager.setUpdating(true);
            }
        }

        gameScoreboardManager.update(true);
    }

    public Map<Class<? extends GameClass>, GameClass> getGameClasses() {
        return gameClasses;
    }

    public static GameLoop getInstance() {
        return instance;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (phase instanceof InProgressPhase || maintenanceEnabled) {
            return;
        }
        
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (phase instanceof InProgressPhase || maintenanceEnabled) {
            return;
        }
        
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Participant participant = getParticipant(event.getPlayer().getUniqueId());
        if (event.getPlayer().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
            if (event.getClickedBlock() != null) {
                Material material = event.getClickedBlock().getType();
                if (material == Material.BEACON || material == Material.CHEST || material == Material.FURNACE
                        || material == Material.BURNING_FURNACE || material == Material.ENCHANTMENT_TABLE
                        || material == Material.ENCHANTED_BOOK || material == Material.WORKBENCH
                        || material == Material.HOPPER || material == Material.ANVIL
                        || material == Material.BREWING_STAND || material == Material.CAKE
                        || material == Material.DISPENSER || material == Material.DROPPER
                        || material == Material.ENDER_CHEST || material == Material.COMMAND
                        || material == Material.ITEM_FRAME) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onHangingBreak(HangingBreakEvent event) {
        if (maintenanceEnabled == false) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onHangingPlace(HangingPlaceEvent event) {
        if (maintenanceEnabled == false) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (maintenanceEnabled == false) {
            if (event.getRightClicked() instanceof ItemFrame) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0) || (phase instanceof InProgressPhase) == false) {
            if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
                if (event.getEntity() instanceof Player) {
                    teleportToLobby((Player) event.getEntity());
                } else {
                    event.getEntity().remove();
                }
            }

            event.setCancelled(true);
            return;
        }
    }
    
    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0)  || phase instanceof StartingPhase) {
            event.setCancelled(true);
            return;
        }
        
        if (event.getDamager() instanceof Player) {
            Participant participant = getParticipant(event.getDamager().getUniqueId());
            if (participant.isAlive() == false) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent event) {
        if (event.getEntity() instanceof  Player) {
            return;
        }
        
        if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0) || maintenanceEnabled || phase instanceof StartingPhase) {
            event.getEntity().remove();
        }
    }

    @EventHandler
    public void onPlayerConsume(PlayerItemConsumeEvent event) {
        Participant participant = getParticipant(event.getPlayer().getUniqueId());
        if (event.getPlayer().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0)) {
            event.setCancelled(true);
            return;
        }
        
        if (event.getEntity().getShooter() instanceof Player) {
            Participant participant = getParticipant(((Player) event.getEntity().getShooter()).getUniqueId());
            if (phase instanceof StartingPhase || Bukkit.getWorlds().get(0) == event.getEntity().getWorld()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        Participant participant = getParticipant(event.getPlayer().getUniqueId());
        if (event.getPlayer().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
            event.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        Participant participant = getParticipant(event.getPlayer().getUniqueId());
        if (event.getPlayer().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onFoodLevelChangeEvent(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            Participant participant = getParticipant(event.getEntity().getUniqueId());
            if (event.getEntity().getWorld() == Bukkit.getWorlds().get(0) || phase instanceof StartingPhase) {
                event.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLogin(AsyncPlayerPreLoginEvent event) {
        NetworkPlayer bp = CoreAPI.getPlayer(event.getUniqueId());
        if (bp != null) {
            if (maintenanceEnabled) {
                if (bp.getRank().ordinal() < Rank.MOD.ordinal()) {
                    event.setKickMessage("\nThis server is currently under maintenance!");
                    event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
                }
            }
            
            if (Bukkit.getOnlinePlayers().size() == Bukkit.getMaxPlayers() && GameLoop.getInstance().getPhase() instanceof InProgressPhase == false && bp.getRank().ordinal() > Rank.DEFAULT.ordinal()) {
                NetworkPlayer candidate = null;
                for (NetworkPlayer NetworkPlayer : CoreAPI.getNetworkPlayers()) {
                    if (NetworkPlayer.getRank() == Rank.DEFAULT) {
                        if (candidate == null) {
                            candidate = NetworkPlayer;
                            continue;
                        }
                        
                        if (NetworkPlayer.getJoinServerTime() > candidate.getJoinServerTime()) {
                            candidate = NetworkPlayer;
                        }
                    }
                }
                
                if (candidate != null) {
                    final Player player = Bukkit.getPlayer(candidate.getUuid());
                    if (player != null) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> player.kickPlayer(FontColor.translateString("\n&6We're sorry, but a donor has taken your place.\nYou can buy a rank at &cstore.thecomet.co")));
                    }
                    event.setLoginResult(AsyncPlayerPreLoginEvent.Result.ALLOWED);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void lowestPlayerJoin(PlayerJoinEvent event) {
        participants.put(event.getPlayer().getUniqueId(), new Participant(event.getPlayer().getUniqueId()));
        cleanPlayer(event.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void highestPlayerJoin(PlayerJoinEvent event) {
        if ((phase instanceof StartingPhase) == false && (phase instanceof InProgressPhase) == false) {
            teleportToLobby(event.getPlayer());
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void monitorPlayerJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void monitorPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        
        participants.remove(event.getPlayer().getUniqueId());
        permissions.remove(event.getPlayer().getUniqueId());

        if (Bukkit.getOnlinePlayers().size() - 1 == 0) {
            if (phase instanceof PreGamePhase == false) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> setPhase(new PreGamePhase(true)), 1);
            }
        }

        WorldCleaner.clearPlayer(event.getPlayer());
    }
    
    @EventHandler
    public void onBlockGrow(BlockGrowEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler
    public void onBlockSpread(BlockSpreadEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler
    public void onWorldLoad(WorldLoadEvent event) {
        event.getWorld().setAutoSave(false);
    }
    
    @EventHandler
    public void onPostMapLoad(PostMapLoadEvent event) {
        event.getWorld().setGameRuleValue("doMobSpawning", "false");
        
        for (Entity entity : event.getWorld().getEntities()) {
            if (entity instanceof Player == false) {
                entity.remove();
            }
        }
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        if (event.getWorld() == Bukkit.getWorlds().get(0)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPreMapLoad(PreMapLoadEvent event) {
        event.getWorld().generator(new VoidGenerator());
    }

    @EventHandler
    public void onBlockFade(BlockFadeEvent event) {
        if (event.getBlock().getType() == Material.ICE || event.getBlock().getType() == Material.PACKED_ICE ||
                event.getBlock().getType() == Material.SNOW || event.getBlock().getType() == Material.SNOW_BLOCK) {
            event.setCancelled(true);
        }
    }
    
    public void teleportToLobby(Player player) {
        player.teleport(SurvivalGames.getSGConfig().spawn.getLocation());
    }
    
    public void teleportAllToLobby() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            teleportToLobby(player);
        }
    }
    
    public void cleanPlayer(Player player) {
        if (participants.containsKey(player.getUniqueId())) {
            if (permissions.containsKey(player.getUniqueId()) == false) {
                permissions.put(player.getUniqueId(), player.addAttachment(CoreAPI.getPlugin()));
            }

            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }

            player.setHealth(20.0);
            player.setFoodLevel(20);
            player.setLevel(0);
            player.setExp(0);
            player.getInventory().setArmorContents(null);
            player.getInventory().clear();
            player.updateInventory();
            player.setGameMode(GameMode.SURVIVAL);
            player.setAllowFlight(false);
            player.setFlying(false);

            Participant participant = participants.get(player.getUniqueId());
            if ((phase instanceof StartingPhase || phase instanceof InProgressPhase) == false) {
                participant.setGameClass(new Tribute());
                participant.setPedestal(null);
            }

            participant.setAlive(true);
        }
    }

    public boolean isMaintenanceEnabled() {
        return maintenanceEnabled;
    }

    public void setMaintenanceEnabled(boolean maintenanceEnabled) {
        this.maintenanceEnabled = maintenanceEnabled;
    }

    public GameScoreboardManager getGameScoreboardManager() {
        return gameScoreboardManager;
    }
}
