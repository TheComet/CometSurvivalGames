package co.thecomet.sg.game;

import co.thecomet.core.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import java.util.List;

public abstract class Phase {
    protected GameLoop loop;
    protected boolean completed;
    
    public Phase() {
        this.loop = GameLoop.getInstance();
        this.completed = false;
        
        if (this instanceof Listener) {
            Bukkit.getPluginManager().registerEvents((Listener) this, CoreAPI.getPlugin());
        }
    }

    public abstract void tick();
    
    public boolean hasCompleted() {
        return this.completed;
    }
    
    protected void setCompleted() {
        this.completed = true;
    }
    
    public abstract Phase next();

    public void cleanup() {}
    
    public abstract List<String> getPhaseInfo();
}
