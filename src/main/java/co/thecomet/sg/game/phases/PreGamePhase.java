package co.thecomet.sg.game.phases;

import co.thecomet.core.listeners.ConnectionListener;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.minigameapi.world.MapLoadException;
import co.thecomet.sg.game.Phase;
import co.thecomet.sg.config.SGMapConfig;
import co.thecomet.sg.game.ui.ClassSelectionMenu;
import co.thecomet.sg.game.ui.MapSelectionMenu;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class PreGamePhase extends Phase implements Listener {
    private final int PLAYER_COUNT_START = 12;
    private final int PLAYER_COUNT_REDUCE = 16;
    private final int COUNTER_START_TIME = 2 * 60;
    private final int COUNTER_REDUCE_TIME = 60;
    private final int COUNTER_END_VOTING = 10;
    
    private LobbyStatus status = LobbyStatus.WAITING;
    private int counter = COUNTER_START_TIME;
    private MapSelectionMenu mapMenu;
    private ClassSelectionMenu classMenu;
    private LoadedMap<SGMapConfig> winner = null;
    private boolean loadMapSuccess = false;
    
    private PreGamePhase() {
        loop.teleportAllToLobby();
        
        mapMenu = new MapSelectionMenu(loop.getMapManager());
        classMenu = new ClassSelectionMenu();

        for (Player player : Bukkit.getOnlinePlayers()) {
            loop.cleanPlayer(player);
            initPlayerInventory(player);
        }

        ConnectionListener.setAnnounceJoin(true);
    }

    public PreGamePhase(boolean unload) {
        this();

        if (unload) {
            loop.getMapManager().clean();
        }
    }
    
    @Override
    public void tick() {
        if (loop.getMapManager().getMaps().size() < 1) {
            return;
        }
        
        int playersOnline = Bukkit.getOnlinePlayers().size();
        if (playersOnline < PLAYER_COUNT_START && status == LobbyStatus.WAITING) {
            return;
        } else if (playersOnline >= PLAYER_COUNT_START && status == LobbyStatus.WAITING) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                MessageFormatter.sendInfoMessage(player, "The game will start in " + (COUNTER_START_TIME / 60) + " minutes");
            }
            
            status = LobbyStatus.VOTING;
        }
        
        if (status == LobbyStatus.VOTING) {
            if (playersOnline >= PLAYER_COUNT_REDUCE && counter > COUNTER_REDUCE_TIME) {
                counter = COUNTER_REDUCE_TIME;
            }
            
            if (counter == COUNTER_END_VOTING) {
                winner = getWinningMap();
                loop.getMapManager().setCurrent(winner);

                for (Player player : Bukkit.getOnlinePlayers()) {
                    MessageFormatter.sendInfoMessage(player, winner.getMapName() + " has been selected as this rounds map!");
                }
                
                status = LobbyStatus.STARTING;
                loop.getMapManager().setUpdating(false);
                try {
                    loop.getMapManager().loadSelectedMap(winner);
                    loadMapSuccess = true;
                } catch (MapLoadException e) {
                    loop.getMapManager().setUpdating(true);
                    setCompleted();
                    return;
                }
            }
        }
        
        if (status == LobbyStatus.STARTING) {
            if (counter == 1) {
                setCompleted();
                return;
            }
        }
        
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.setLevel(counter);
        }
        
        counter--;
        loop.getGameScoreboardManager().update(false);
    }

    @Override
    public Phase next() {
        return loadMapSuccess ? new StartingPhase(false) : new PreGamePhase(false);
    }

    @Override
    public void cleanup() {
        Bukkit.getScheduler().cancelTask(mapMenu.getTaskId());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        initPlayerInventory(event.getPlayer());
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        mapMenu.removeVote(event.getPlayer());
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        event.setCancelled(triggerMenuItem(event.getPlayer(), event.getItem()));
    }
    
    @EventHandler
    public void onGamemodeChange(PlayerGameModeChangeEvent event) {
        if (event.getNewGameMode() == GameMode.SURVIVAL) {
            initPlayerInventory(event.getPlayer());
        }
    }

    public boolean triggerMenuItem(Player player, ItemStack stack) {
        if (stack == null || stack.getData().getItemType() == Material.AIR) {
            return false;
        }
        
        if (mapMenu.getItem().getItemStack().equals(stack)) {
            mapMenu.getItem().onClick(player);
            return true;
        }
        
        if (classMenu.getItem().getItemStack().equals(stack)) {
            classMenu.getItem().onClick(player);
            return true;
        }

        return false;
    }
    
    public void initPlayerInventory(Player player) {
        player.getInventory().setItem(0, mapMenu.getItem().getItemStack());
        player.getInventory().setItem(1, classMenu.getItem().getItemStack());
        player.updateInventory();
    }
    
    public LoadedMap<SGMapConfig> getWinningMap() {
        return mapMenu.getWinningMap();
    }

    public LobbyStatus getStatus() {
        return status;
    }

    public int getCount() {
        return counter;
    }

    public LoadedMap<SGMapConfig> getWinner() {
        return winner;
    }

    @Override
    public List<String> getPhaseInfo() {
        return new ArrayList<String>() {{
            add("Count: " + counter);
            add("Next Map: " + (winner == null ? "n/a" : winner.getMapName()));
        }};
    }

    public MapSelectionMenu getMapMenu() {
        return mapMenu;
    }

    public enum LobbyStatus {
        WAITING,
        VOTING,
        STARTING,
    }
}
