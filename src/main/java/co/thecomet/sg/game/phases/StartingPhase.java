package co.thecomet.sg.game.phases;

import co.thecomet.core.chat.MessageService;
import co.thecomet.core.config.JsonLocation;
import co.thecomet.core.listeners.ConnectionListener;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.sg.game.Phase;
import co.thecomet.sg.config.SGMapConfig;
import co.thecomet.sg.game.player.Participant;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StartingPhase extends Phase implements Listener {
    private final int COUNT_START = 15;
    private final LoadedMap<SGMapConfig> map;
    private final List<JsonLocation> pedestals;

    private boolean testing = false;
    private int count = COUNT_START;

    public StartingPhase(boolean testing) {
        this.testing = testing;
        this.map = loop.getMapManager().getCurrent();
        this.pedestals = new ArrayList<>(map.getConfig().pedestals);

        ConnectionListener.setAnnounceJoin(false);
    }

    @Override
    public void tick() {
        World world = Bukkit.getWorld(loop.getMapManager().getCurrent().getWorld().getName());
        if (world == null) {
            return;
        }

        if (count == COUNT_START) {
            world.setTime(6000);
            world.setStorm(false);
            world.getWorldBorder().reset();
            Bukkit.getOnlinePlayers().forEach((player) -> cleanPlayer(player));
            Bukkit.getOnlinePlayers().forEach(this::teleport);
        }

        if (((count % 5 == 0 && count >= 5) || count < 5) && count > 0) {
            MessageService.sendTitleAnnouncement(String.valueOf(count));
        }

        if (count == 1) {
            setCompleted();
            return;
        }

        count--;
        loop.getGameScoreboardManager().update(false);
    }

    @Override
    public Phase next() {
        return new InProgressPhase(testing);
    }

    public void cleanPlayer(Player player) {
        loop.cleanPlayer(player);
    }

    public void teleport(Player player) {
        Participant participant = loop.getParticipant(player.getUniqueId());
        if (map.getConfig().pedestals.isEmpty()) {
            player.teleport(Bukkit.getWorld(loop.getMapManager().getCurrent().getWorld().getName()).getSpawnLocation());
        } else {
            player.teleport(pedestals.get(0).getLocation());
            participant.setPedestal(pedestals.get(0));
            Collections.rotate(pedestals, 1);
        }
    }

    public int getCount() {
        return count;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.getTo().getBlockX() != event.getFrom().getBlockX() || event.getTo().getBlockZ() != event.getFrom().getBlockZ()) {
            Participant participant = loop.getParticipant(event.getPlayer().getUniqueId());
            if (participant.getPedestal() != null) {
                event.getPlayer().teleport(participant.getPedestal().getLocation().setDirection(event.getPlayer().getLocation().getDirection()));
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        cleanPlayer(event.getPlayer());
        teleport(event.getPlayer());
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
        event.setCancelled(true);
    }

    @Override
    public List<String> getPhaseInfo() {
        return new ArrayList<String>() {{
            add("Testing: " + testing);
            add("Count: " + count);
        }};
    }
}
