package co.thecomet.sg.game.phases;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.chat.MessageService;
import co.thecomet.core.config.JsonLocation;
import co.thecomet.core.db.CoreDataAPI;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.items.ItemHelper;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.sg.SurvivalGames;
import co.thecomet.sg.config.ChestItem;
import co.thecomet.sg.config.SGConfig;
import co.thecomet.sg.game.Phase;
import co.thecomet.sg.game.classes.ActiveBaseGameClass;
import co.thecomet.sg.config.SGMapConfig;
import co.thecomet.sg.game.player.Participant;
import org.bukkit.*;
import org.bukkit.block.Chest;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InProgressPhase extends Phase implements Listener {
    private final int COUNT_START = 60 * 15;
    private final int COUNT_GRACE_PERIOD = 10;
    private final int COUNT_CLASS_DELAY = 30;
    private final int COUNT_SIX_REMAINING = 60 * 2;
    private final int COUNT_FOUR_REMAINING = 30;
    private final int COUNT_DEATHMATCH_TIME = 60;
    
    private boolean testing = false;
    private int count = COUNT_START;
    private SGMapConfig config;
    private List<JsonLocation> chests;
    private List<Participant> participants;
    private Participant winner;
    private boolean deathmatch = false;
    
    public InProgressPhase(boolean testing) {
        this.testing = testing;
        this.config = loop.getMapManager().getCurrent().getConfig();
        this.chests = new ArrayList<>(config.chests);
        this.participants = loop.getParticipants();

        participants.forEach(participant -> participant.setAlive(true));
    }
    
    @Override
    public void tick() {
        if (participants.size() == 1 && (testing == false || (testing && count < COUNT_START - COUNT_DEATHMATCH_TIME))) {
            winner = participants.get(0);
        }
        
        if (winner != null || participants.size() == 0) {
            World world = Bukkit.getWorld(loop.getMapManager().getCurrent().getWorld().getName());
            world.getWorldBorder().setSize(60000000);
            
            setCompleted();
            return;
        }
        
        if (count == COUNT_START) {
            MessageService.sendFullTitleAnnouncement("BEGIN", "May the odds be in your favor!", 20, 60, 20);
            
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 30, 1));
                player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 30, 1));
                player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 20 * 30, 1));
            }
        }
        
        if (count == COUNT_START - COUNT_CLASS_DELAY) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                Participant participant = loop.getParticipant(player.getUniqueId());
                if (participant != null && participant.getGameClass() != null) {
                    if (participant.getGameClass().getStartingItems() != null && participant.getGameClass().getStartingItems().size() > 0) {
                        for (ItemStack is : participant.getGameClass().getStartingItems()) {
                            if (ItemHelper.isArmor(is)) {
                                ItemHelper.equipArmor(player, is);
                            } else {
                                player.getInventory().addItem(is);
                            }
                        }
                        
                        player.updateInventory();
                    }
                    
                    if (participant.getGameClass().getStartingEffects() != null && participant.getGameClass().getStartingEffects().size() > 0) {
                        player.addPotionEffects(participant.getGameClass().getStartingEffects());
                    }
                }
            }
        }
        
        if (count < COUNT_START - COUNT_CLASS_DELAY) {
            loop.getGameClasses().values().stream().filter(gameClass -> gameClass instanceof ActiveBaseGameClass).forEach(gameClass -> {
                ActiveBaseGameClass activeBaseGameClass = (ActiveBaseGameClass) gameClass;
                if (activeBaseGameClass.getProcessor() != null) {
                    activeBaseGameClass.loop();
                }
            });
            
            if (participants.size() <= 6 && count > COUNT_SIX_REMAINING) {
                if (testing == false || (testing && count == (COUNT_START - COUNT_SIX_REMAINING))) {
                    count = COUNT_SIX_REMAINING;
                }
            }
            
            if (count == COUNT_SIX_REMAINING) {
                MessageService.sendFullTitleAnnouncement((COUNT_SIX_REMAINING / 60) + " Minutes", "Until Deathmatch!");
            }
            
            if (participants.size() <= 4 && count > COUNT_FOUR_REMAINING) {
                if (testing == false || (testing && count == (COUNT_START - COUNT_SIX_REMAINING - COUNT_FOUR_REMAINING))) {
                    count = COUNT_FOUR_REMAINING;
                }
            }
            
            if (count == COUNT_FOUR_REMAINING) {
                MessageService.sendFullTitleAnnouncement(COUNT_FOUR_REMAINING + " Seconds", "Until Deathmatch!");
            }

            if (count == 0) {
                LoadedMap<SGMapConfig> map = loop.getMapManager().getCurrent();
                World world = Bukkit.getWorld(map.getWorld().getName());
                if (deathmatch == false) {
                    Location location = world.getSpawnLocation();
                    if (config.arenaCenter != null) {
                        location = config.arenaCenter.getLocation();
                    }

                    world.getWorldBorder().setCenter(location);
                    world.getWorldBorder().setSize(150.0);
                    world.getWorldBorder().setSize(0.0, COUNT_DEATHMATCH_TIME);
                    world.getWorldBorder().setDamageAmount(0.5);
                    world.getWorldBorder().setDamageBuffer(0.0);
                    deathmatch = true;

                    if (config.spectatorSpawn != null) {
                        location = config.spectatorSpawn.getLocation();
                    }
                    
                    for (Participant participant : loop.getParticipants()) {
                        Player player = Bukkit.getPlayer(participant.getUuid());
                        if (player != null) {
                            if (participant.isAlive()) {
                                player.teleport(participant.getPedestal().getLocation());
                            } else {
                                player.teleport(location);
                            }
                        }
                    }
                } else {
                    if (Bukkit.getWorld(map.getWorld().getName()).getWorldBorder().getSize() == 1.0) {
                        Location location = map.getConfig().arenaCenter.getLocation();
                        location.add(0, 10, 0);

                        if (location.getBlock().getType() != Material.LAVA) {
                            location.getBlock().setType(Material.LAVA);
                        }
                    }
                }

                return;
            }
        }
        
        count--;
        loop.getGameScoreboardManager().update(false);
    }

    @Override
    public Phase next() {
        return winner != null ? new EndGamePhase(winner) : new EndGamePhase();
    }

    @Override
    public void cleanup() {
        winner = null;
    }

    public void populateChest(Chest chest) {
        SGConfig config = SurvivalGames.getSGConfig();
        chest.getInventory().clear();

        int points = 0;
        int attempts = 20;
        int chanceAttempts = 5;
        boolean complete = false;
        List<ChestItem> items = new ArrayList<>(config.chestItems);

        while (!complete) {
            boolean fullChancePresent = false;
            // Remove items that require more points than are remaining
            for (ChestItem item : new ArrayList<>(items)) {
                if (item.requiredChestPoints > config.maxChestPoints - points) {
                    items.remove(item);
                    continue;
                }

                if (item.rarity == 100) {
                    fullChancePresent = true;
                } else {
                    if (chanceAttempts == 0) {
                        items.remove(item);
                    }
                }
            }

            // Check if we have any items left to attempt to fill the chest with
            if (items.isEmpty() || (attempts == 0 && points > config.minChestPoints) || (chanceAttempts == 0 && attempts == 0 && fullChancePresent == false)) {
                break;
            }


            ChestItem item = items.get(0);
            if (item.rarity < 100) {
                chanceAttempts--;
            }
            
            if (item.rarity == 100 || GeneralUtils.RANDOM.nextDouble() <= item.rarity) {
                points += item.requiredChestPoints;
                int maxSlot = chest.getInventory().getSize() - 1;
                int slot = GeneralUtils.getBetween(1, (maxSlot < 54 ? maxSlot + 1 : GeneralUtils.getBetween(1, 27)) - 1);
                ItemStack is = chest.getInventory().getItem(slot);

                while (is != null) {
                    slot = GeneralUtils.getBetween(1, (maxSlot < 54 ? maxSlot + 1: GeneralUtils.getBetween(1, 27)) - 1);
                    is = chest.getInventory().getItem(slot);
                }

                chest.getInventory().setItem(slot, item.getItemStack());
            }

            Collections.shuffle(items, GeneralUtils.RANDOM);
            attempts--;
        }
    }

    private static Player getKiller(PlayerDeathEvent event) {
        Player p = event.getEntity();

        if (p.isDead()) {
            if (p.getKiller() instanceof Player) {
                return (Player) p.getKiller();
            }
        }

        return null;
    }

    public int getCount() {
        return count;
    }

    public List<Participant> getParticipants() {
        return participants;
    }
    
    @EventHandler
    public void onCraftItem(CraftItemEvent event) {
        if (event.getRecipe().getResult().getType() == Material.BUCKET) {
            event.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onChestOpen(InventoryOpenEvent event) {
        if (event.getInventory().getHolder() instanceof Chest) {
            Chest chest = (Chest) event.getInventory().getHolder();
            for (JsonLocation location : new ArrayList<>(chests)) {
                if (chest.getLocation().equals(location.getLocation())) {
                    chests.remove(location);
                    populateChest((Chest) event.getInventory().getHolder());
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        Participant participant = loop.getParticipant(player.getUniqueId());
        participant.setAlive(false);

        Player killer;
        if ((killer = getKiller(event)) != null) {
            NetworkPlayer bp = CoreAPI.getPlayer(killer);
            int points = 1 * (bp.getRank().ordinal() >= Rank.ULTRA.ordinal() ? 3 : bp.getRank().ordinal() >= Rank.VIP.ordinal() ? 2 : 1);
            CoreDataAPI.addPoints(points, killer, false);
            MessageFormatter.sendInfoMessage(killer, "You have received " + points + " points for killing " + player.getName());
        }

        player.teleport(participant.getPedestal().getLocation());

        participants.remove(participant);
        MessageService.sendTitleAnnouncement(event.getEntity(), FontColor.translateString("&cYOU DIED"));
        MessageService.actionAnnouncement(FontColor.translateString("&6" + player.getName()+ " HAS FALLEN, " + participants.size() + " TRIBUTES REMAINING"));

        if (player.isInsideVehicle()) {
            player.leaveVehicle();
        }

        player.setHealth(20);
        player.setFoodLevel(20);
        player.setGameMode(GameMode.SPECTATOR);

        event.setDeathMessage(null);
        Bukkit.getPluginManager().callEvent(new PlayerRespawnEvent(event.getEntity(), Bukkit.getWorld(loop.getMapManager().getCurrent().getWorld().getName()).getSpawnLocation(), false));
        loop.getGameScoreboardManager().update(false);
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Entity entityDamager = event.getDamager();
        Entity entityDamaged = event.getEntity();
        
        if (entityDamaged instanceof Player && entityDamager instanceof Player) {
            if (count >= COUNT_START - COUNT_GRACE_PERIOD) {
                event.setCancelled(true);
            }
        }

        if(entityDamager instanceof Arrow) {
            if(entityDamaged instanceof Player && ((Arrow) entityDamager).getShooter() instanceof Player) {
                Arrow arrow = (Arrow) entityDamager;

                Vector velocity = arrow.getVelocity();

                Player shooter = (Player) arrow.getShooter();
                Player damaged = (Player) entityDamaged;

                if(isAlive(damaged) == false) {
                    damaged.teleport(entityDamaged.getLocation().add(0, 5, 0));

                    Arrow newArrow = shooter.launchProjectile(Arrow.class);
                    newArrow.setShooter(shooter);
                    newArrow.setVelocity(velocity);
                    newArrow.setBounce(false);

                    event.setCancelled(true);
                    arrow.remove();
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        if (loop.getMapManager().getCurrent().getConfig().spectatorSpawn != null) {
            event.setRespawnLocation(loop.getMapManager().getCurrent().getConfig().spectatorSpawn.getLocation());
        }
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Location location;
        if (loop.getMapManager().getCurrent().getConfig().spectatorSpawn != null) {
            location = loop.getMapManager().getCurrent().getConfig().spectatorSpawn.getLocation();
        } else {
            location = Bukkit.getWorld(loop.getMapManager().getCurrent().getWorld().getName()).getSpawnLocation();
        }

        Participant participant = loop.getParticipant(event.getPlayer().getUniqueId());
        participant.setAlive(false);

        event.getPlayer().teleport(location);
        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> event.getPlayer().setGameMode(GameMode.SPECTATOR), 1);
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        for (Participant participant : new ArrayList<>(participants)) {
            if (participant.getUuid() == event.getPlayer().getUniqueId()) {
                participants.remove(participant);
            }
        }
    }
    
    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (isAlive(event.getPlayer()) == false) {
            event.setFormat("&8[&7SPEC&8] " + event.getFormat());
            for (Participant part : participants) {
                Player player = Bukkit.getPlayer(part.getUuid());
                event.getRecipients().remove(player);
            }
        }
    }
    
    @EventHandler
    public void onTarget(EntityTargetLivingEntityEvent event) {
        if (event.getTarget() instanceof Player) {
            if (isAlive((Player) event.getTarget()) == false) {
                event.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (isAlive(event.getPlayer()) == false) {
            event.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Participant participant = loop.getParticipant(event.getPlayer().getUniqueId());
        if (participants.contains(participant)) {
            if (event.getBlock().getType() == Material.LEAVES || event.getBlock().getType() == Material.SPONGE) {
                return;
            }
        }
        
        event.setCancelled(true);
    }
    
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Participant participant = loop.getParticipant(event.getPlayer().getUniqueId());
        if (participants.contains(participant)) {
            if (event.getBlock().getType() == Material.CAKE_BLOCK || event.getBlock().getType() == Material.CAKE) {
                return;
            }
        }
        
        event.setCancelled(true);
    }
    
    public boolean isAlive(Player player) {
        Participant participant = loop.getParticipant(player.getUniqueId());
        for (Participant part : participants) {
            if (participant == part) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public List<String> getPhaseInfo() {
        return new ArrayList<String>() {{
            add("Testing: " + testing);
            add("Count: " + count);
            add("Participants Remaining: " + participants.size());
            add("Deathmatch: " + deathmatch);
            add("Winner: " + (winner == null ? "n/a" : winner.getName()));
        }};
    }
}
