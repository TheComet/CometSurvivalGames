package co.thecomet.sg.game.phases;

import co.thecomet.sg.game.Phase;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;

public class MaintenancePhase extends Phase implements Listener {
    @Override
    public void tick() {
        // Nothing to do here really
    }

    @Override
    public Phase next() {
        return new PreGamePhase(true);
    }

    @Override
    public List<String> getPhaseInfo() {
        return new ArrayList<String>() {{
            add("Maintenance Mode!");
        }};
    }
}
