package co.thecomet.sg.game.phases;

import co.thecomet.core.chat.MessageService;
import co.thecomet.core.db.CoreDataAPI;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.sg.game.Phase;
import co.thecomet.sg.game.player.Participant;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class EndGamePhase extends Phase {
    private final int COUNT_START = 5;
    private final int WIN_POINTS = 15;

    private int count = COUNT_START;
    private Participant winner;
    
    public EndGamePhase(Participant winner) {
        this.winner = winner;
    }
    
    public EndGamePhase() {
        winner = null;
    }
    
    @Override
    public void tick() {
        if (count == COUNT_START) {
            if (winner != null) {
                Player player = Bukkit.getPlayer(winner.getUuid());
                CoreDataAPI.addPoints(WIN_POINTS, player, false);
                MessageFormatter.sendInfoMessage(player, "You have received " + WIN_POINTS + " points for winning!");
            }

            Bukkit.getOnlinePlayers().forEach(p -> {
                p.setGameMode(GameMode.SPECTATOR);
                MessageService.sendTitleAnnouncement(p, winner != null ? (winner.getName() + " WINS!") : "No Winner");
            });
        }

        if (count == 0) {
            setCompleted();
            return;
        }

        count--;
        loop.getGameScoreboardManager().update(false);
    }

    @Override
    public void cleanup() {
        winner = null;
    }

    @Override
    public Phase next() {
        return new PreGamePhase(true);
    }

    public int getCount() {
        return count;
    }

    public Participant getWinner() {
        return winner;
    }

    @Override
    public List<String> getPhaseInfo() {
        return new ArrayList<String>() {{
            add("Count: " + count);
            add("Winner: " + (winner == null ? "n/a" : winner.getName()));
        }};
    }
}
