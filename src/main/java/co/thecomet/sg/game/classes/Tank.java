package co.thecomet.sg.game.classes;

import co.thecomet.core.transaction.CurrencyType;
import co.thecomet.core.transaction.Feature;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class Tank extends BaseGameClass {
    public Tank() {
        super("Tank", Material.IRON_CHESTPLATE, 5);
        
        this.points = 5000;
        this.feature = new Feature("Tank", "sg-class-tank", points, true, CurrencyType.POINTS);
        
        this.startingItems.add(ItemBuilder.build(Material.STONE_SWORD).build());
        this.startingItems.add(ItemBuilder.build(Material.IRON_HELMET).build());
        this.startingItems.add(ItemBuilder.build(Material.IRON_CHESTPLATE).build());
        
        this.startingEffects.add(new PotionEffect(PotionEffectType.SLOW, 20 * 60, 0));
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add("&6You will start out with:");
            add(" &7- &eStone Sword");
            add(" &7- &eIron Helmet");
            add(" &7- &eIron Chestplate");
            add(" &7- &eEffect: Slowness 1, 4 Minutes");
        }});
    }
}
