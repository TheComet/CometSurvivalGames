package co.thecomet.sg.game.classes;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.transaction.Feature;
import co.thecomet.core.transaction.Transaction;
import co.thecomet.core.transaction.TransactionManager;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.sg.game.GameLoop;
import co.thecomet.sg.game.phases.PreGamePhase;
import co.thecomet.sg.game.player.Participant;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseGameClass implements GameClass {
    protected MenuItem menuItem;
    protected List<ItemStack> startingItems = new ArrayList<>();
    protected List<PotionEffect> startingEffects = new ArrayList<>();
    protected int points = 0;
    protected Rank requiredRank = Rank.DEFAULT;
    protected Feature feature = null;
    
    public BaseGameClass(String menuDisplay, Material menuMaterial, int menuSlot) {
        menuItem = new MenuItem(menuDisplay, new MaterialData(menuMaterial)) {
            @Override
            public void onClick(Player player) {
                if (GameLoop.getInstance().getPhase() instanceof PreGamePhase) {
                    NetworkPlayer bp = CoreAPI.getPlayer(player);
                    if (bp.getRank().ordinal() < getRequiredRank().ordinal()) {
                        MessageFormatter.sendErrorMessage(player, "This class requires that you be " + getRequiredRank().getDisplayName() + " or higher.");
                        return;
                    }
                    
                    if (feature != null && bp.getRank().ordinal() < Rank.ULTRA.ordinal()) {
                        boolean purchased = false;
                        for (Transaction transaction : new ArrayList<>(bp.getTransactions())) {
                            if (transaction.feature.equals(feature)) {
                                purchased = true;
                                break;
                            }
                        }
                        
                        if (purchased == false) {
                            TransactionManager.submit(player, feature);
                            return;
                        }
                    }
                    
                    Participant participant = GameLoop.getInstance().getParticipant(player.getUniqueId());
                    if (participant.getGameClass() == BaseGameClass.this) {
                        MessageFormatter.sendErrorMessage(player, "You have already selected that class!");
                    } else {
                        participant.setGameClass(BaseGameClass.this);
                        MessageFormatter.sendSuccessMessage(player, "You have chosen to be a " + getMenuItem().getText());
                    }
                }
            }
        };
        menuItem.setSlot(menuSlot);
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    @Override
    public List<ItemStack> getStartingItems() {
        return startingItems;
    }

    @Override
    public int getPoints() {
        return points;
    }

    @Override
    public List<PotionEffect> getStartingEffects() {
        return startingEffects;
    }

    @Override
    public Rank getRequiredRank() {
        return requiredRank;
    }

    public void setMenuItemDescription(List<String> description) {
        List<String> colorized = new ArrayList<>();
        for (String line : description) {
            colorized.add(FontColor.translateString(line));
        }
        menuItem.setDescriptions(colorized);
    }
}
