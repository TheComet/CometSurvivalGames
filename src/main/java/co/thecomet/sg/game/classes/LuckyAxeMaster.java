package co.thecomet.sg.game.classes;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.utils.items.ItemBuilder;
import co.thecomet.sg.game.GameLoop;
import co.thecomet.sg.game.player.Participant;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class LuckyAxeMaster extends BaseGameClass implements Listener {
    public LuckyAxeMaster() {
        super("Lucky Axe Master", Material.GOLD_AXE, 19);
        
        this.requiredRank = Rank.VIP;
        
        this.startingItems.add(ItemBuilder.build(Material.GOLD_AXE)
                .name(FontColor.translateString("&eLucky Axe"))
                .enchantment(Enchantment.DAMAGE_ALL).build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &eLucky Axe:");
            add("   &2+ &8Sharpness 1");
            add("   &2+ &85% Chance To Inflict Nausea");
        }});
    }
    
    @EventHandler
    public void onAttack(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            Participant participant = GameLoop.getInstance().getParticipant(event.getDamager().getUniqueId());

            if (participant == null || participant.getGameClass() == null) {
                return;
            }
            
            if (participant.getGameClass() == this) {
                if (GeneralUtils.getBetween(0, 100) < 5) {
                    ((Player) event.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 * 15, GeneralUtils.getBetween(0, 2)));
                }
            }
        }
    }
}
