package co.thecomet.sg.game.classes;

import co.thecomet.core.transaction.CurrencyType;
import co.thecomet.core.transaction.Feature;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class Scout extends BaseGameClass {
    public Scout() {
        super("Scout", Material.COMPASS, 3);
        
        this.points = 5000;
        this.feature = new Feature("Scout", "sg-class-scout", points, true, CurrencyType.POINTS);
        
        this.startingItems.add(ItemBuilder.build(Material.GOLD_SWORD).build());
        this.startingItems.add(ItemBuilder.build(Material.POTION).name("Speed II").effect(PotionEffectType.SPEED, 20 * 60 * 1).build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add("&6You will start out with:");
            add(" &7- &eGold Sword");
            add(" &7- &ePotion: Speed 1, 2 Minutes");
        }});
    }
}
