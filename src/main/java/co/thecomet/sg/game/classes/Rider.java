package co.thecomet.sg.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.effects.particle.ParticleEffect;
import co.thecomet.core.utils.items.ItemBuilder;
import co.thecomet.sg.game.GameLoop;
import co.thecomet.sg.game.player.Participant;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Rider extends BaseGameClass implements Listener {
    private Cache<UUID, Long> cooldown = CacheBuilder.newBuilder().expireAfterWrite(30, TimeUnit.SECONDS).build();
    
    public Rider() {
        super("Rider", Material.DIAMOND_BARDING, 25);
        
        this.requiredRank = Rank.ULTRA;
        
        this.startingItems.add(ItemBuilder.build(Material.STICK).name("Pony Flute").build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &ePony Flute:");
            add("   &2+ &8Summons A Magical Pony!");
            add("   &2+ &830 Second Cooldown");
            add("   &2+ &8Despawns On Dismount");
            add("   &2+ &8Disabled During Deathmatch");
        }});
    }
    
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Participant participant = GameLoop.getInstance().getParticipant(event.getPlayer().getUniqueId());
        if (participant.getGameClass() == this) {
            if (event.getPlayer().isInsideVehicle()) {
                if (event.getPlayer().getVehicle().getCustomName().equalsIgnoreCase("Magical Farting Pony!")) {
                    Location location = event.getPlayer().getLocation();
                    ParticleEffect.REDSTONE.display(0.0f, 0.0f, 0.0f, 1.0f, 5, location, Bukkit.getOnlinePlayers().toArray(new Player[Bukkit.getOnlinePlayers().size()]));
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getItem() == null || event.getItem().getItemMeta() == null || event.getItem().getItemMeta().getDisplayName() == null) {
            return;
        }
        
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        
        Participant participant = GameLoop.getInstance().getParticipant(event.getPlayer().getUniqueId());

        if (participant == null || participant.getGameClass() == null) {
            return;
        }
        
        if (participant.getGameClass() == this) {
            if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("Pony Flute")) {
                if (cooldown.asMap().containsKey(event.getPlayer().getUniqueId())) {
                    return;
                }
                
                Horse horse = (Horse) event.getPlayer().getWorld().spawnEntity(event.getPlayer().getLocation(), EntityType.HORSE);
                horse.setTamed(true);
                horse.setOwner(event.getPlayer());
                horse.setCustomName("Magical Farting Pony!");
                horse.setCustomNameVisible(true);
                horse.setColor(Horse.Color.WHITE);
                horse.setStyle(Horse.Style.NONE);
                horse.setVariant(Horse.Variant.HORSE);
                horse.getInventory().setArmor(ItemBuilder.build(Material.DIAMOND_BARDING).build());
                horse.getInventory().setSaddle(ItemBuilder.build(Material.SADDLE).build());
                horse.setPassenger(event.getPlayer());
                
                cooldown.put(participant.getUuid(), System.currentTimeMillis());
            }
        }
    }
    
    @EventHandler
    public void onVehicleExit(VehicleExitEvent event) {
        if (event.getVehicle() != null && event.getVehicle() instanceof Horse) {
            if (event.getVehicle().getCustomName() != null && event.getVehicle().getCustomName().equalsIgnoreCase("Magical Farting Pony!")) {
                event.getVehicle().remove();
            }
        }
    }
}
