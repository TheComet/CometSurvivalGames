package co.thecomet.sg.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.ui.MenuItem;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.List;

public interface GameClass {
    public MenuItem getMenuItem();
    
    public int getPoints();
    
    public List<ItemStack> getStartingItems();
    
    public List<PotionEffect> getStartingEffects();
    
    public Rank getRequiredRank();
}
