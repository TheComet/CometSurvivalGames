package co.thecomet.sg.game.classes;

import co.thecomet.common.user.Rank;
import co.thecomet.core.utils.items.ItemBuilder;
import co.thecomet.sg.game.GameLoop;
import co.thecomet.sg.game.player.Participant;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;

public class Beastmaster extends BaseGameClass implements Listener {
    public Beastmaster() {
        super("Beastmaster", Material.BONE, 23);
        
        this.requiredRank = Rank.VIP;
        
        this.startingItems.add(ItemBuilder.build(Material.MONSTER_EGG).durability(EntityType.WOLF.getTypeId()).amount(3).build());
        this.startingItems.add(ItemBuilder.build(Material.BONE).amount(10).build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add(getRequiredRank().getDisplayName());
            add("");
            add("&6You will start out with:");
            add(" &7- &e3x Dog Eggs");
            add(" &7- &e10x Bones");
        }});
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Participant participant = GameLoop.getInstance().getParticipant(event.getPlayer().getUniqueId());
        
        if (participant == null || participant.getGameClass() == null) {
            return;
        }
        
        if (participant.getGameClass() == this) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                if (event.getItem() != null && event.getItem().getType() == Material.MONSTER_EGG) {
                    if (event.getItem().getDurability() == EntityType.WOLF.ordinal()) {
                        Tameable wolf = (Tameable) event.getPlayer().getWorld().spawnEntity(event.getPlayer().getLocation(), EntityType.WOLF);
                        wolf.setTamed(true);
                        wolf.setOwner(event.getPlayer());
                        
                        if (event.getItem().getAmount() > 1) {
                            event.getItem().setAmount(event.getItem().getAmount() - 1);
                        } else {
                            event.getPlayer().getInventory().remove(event.getItem());
                        }
                        
                        event.setCancelled(true);
                    }
                }
            }
        }
    }
}
