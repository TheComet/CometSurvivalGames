package co.thecomet.sg.game.classes;

import co.thecomet.core.transaction.CurrencyType;
import co.thecomet.core.transaction.Feature;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;

import java.util.ArrayList;

public class Swordsman extends BaseGameClass {
    public Swordsman() {
        super("Swordsman", Material.WOOD_SWORD, 1);
        
        this.points = 3000;
        this.feature = new Feature("Swordsman", "sg-class-swordsman", points, true, CurrencyType.POINTS);
        
        this.startingItems.add(ItemBuilder.build(Material.WOOD_SWORD).build());
        this.startingItems.add(ItemBuilder.build(Material.COOKED_BEEF).amount(3).build());
        
        this.setMenuItemDescription(new ArrayList<String>() {{
            add("&6You will start out with:");
            add(" &7- &eWooden Sword");
            add(" &7- &e3x Cooked Beef");
        }});
    }
}
