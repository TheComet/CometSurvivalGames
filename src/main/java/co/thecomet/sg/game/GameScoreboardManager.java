package co.thecomet.sg.game;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.utils.scoreboard.SimpleScoreboard;
import co.thecomet.sg.game.phases.EndGamePhase;
import co.thecomet.sg.game.phases.InProgressPhase;
import co.thecomet.sg.game.phases.PreGamePhase;
import co.thecomet.sg.game.phases.StartingPhase;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class GameScoreboardManager implements Listener {
    private final GameLoop LOOP;
    private SimpleScoreboard scoreboard;
    
    public GameScoreboardManager(GameLoop loop) {
        this.LOOP = loop;
        this.update(false);

        Bukkit.getPluginManager().registerEvents(this, CoreAPI.getPlugin());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        scoreboard.send(event.getPlayer());
    }

    public void update(boolean reset) {
        Phase phase = LOOP.getPhase();

        if (scoreboard != null && reset) {
            scoreboard.reset();
        }

        switch (phase.getClass().getSimpleName()) {
            case "PreGamePhase":
                updatePreGameScoreboard((PreGamePhase) phase);
                break;
            case "StartingPhase":
                updateStartingScoreboard((StartingPhase) phase);
                break;
            case "InProgressPhase":
                updateInProgressScoreboard((InProgressPhase) phase);
                break;
            case "EndGamePhase":
                updateEndGameScoreboard((EndGamePhase) phase);
                break;
            default:
                return;
        }

        scoreboard.update();
        scoreboard.send(Bukkit.getOnlinePlayers().toArray(new Player[]{}));
    }

    public void updatePreGameScoreboard(PreGamePhase phase) {
        String title = FontColor.BOLD + "Starting In: " + ChatColor.GOLD + String.valueOf(phase.getCount());

        if (scoreboard == null) {
            scoreboard = new SimpleScoreboard(title);
        } else if (!title.equals(scoreboard.getTitle())) {
            scoreboard.setTitle(title);
        }

        if (!scoreboard.containsScore(5)) {
            scoreboard.add(FontColor.translateString("&6~~~~~~~~~~~~~~&a"), 5);
        }

        if (!scoreboard.containsScore(4)) {
            scoreboard.add(FontColor.translateString("&b&l> &e&lNext Map:"), 4);
        }

        String map = phase.getWinner() == null ? "n/a" : phase.getWinner().getMapName();
        if (!scoreboard.containsScore(3)) {
            scoreboard.add(FontColor.translateString("&f&o") + map, 3);
        } else if (!scoreboard.get(3, null).contains(map)) {
            scoreboard.remove(3, scoreboard.get(3, null));
            scoreboard.add(FontColor.translateString("&f&o") + map, 3);
        }

        if (!scoreboard.containsScore(2)) {
            scoreboard.add(FontColor.translateString("&a"), 2);
        }

        if (!scoreboard.containsScore(1)) {
            scoreboard.add(FontColor.translateString("&6~~~~~~~~~~~~~~&b"), 1);
        }
    }

    public void updateStartingScoreboard(StartingPhase phase) {
        String title = FontColor.BOLD + "Starting In: " + ChatColor.GOLD + String.valueOf(phase.getCount());

        if (!title.equals(scoreboard.getTitle())) {
            scoreboard.setTitle(title);
        }

        if (!scoreboard.containsScore(1)) {
            scoreboard.add(FontColor.translateString("&6~~~~~~~~~~~~~~"), 1);
        }
    }

    public void updateInProgressScoreboard(InProgressPhase phase) {
        String title = FontColor.BOLD + "Deathmatch: " + ChatColor.GOLD + String.valueOf(phase.getCount());

        if (!title.equals(scoreboard.getTitle())) {
            scoreboard.setTitle(title);
        }

        if (!scoreboard.containsScore(4)) {
            scoreboard.add(FontColor.translateString("&6~~~~~~~~~~~~~~&a"), 4);
        }

        if (!scoreboard.containsScore(3)) {
            scoreboard.add(FontColor.translateString("&b&l> &e&lAlive: " + String.valueOf(phase.getParticipants() == null ? 0 : phase.getParticipants().size())), 3);
        } else {
            String value = FontColor.stripColor(scoreboard.get(3, null)).replace("> Alive: ", "");
            if (!value.equals(String.valueOf(phase.getParticipants() == null ? 0 : phase.getParticipants().size()))) {
                scoreboard.remove(3, scoreboard.get(3, null));
                scoreboard.add(FontColor.translateString("&b&l> &e&lAlive: " + String.valueOf(phase.getParticipants() == null ? 0 : phase.getParticipants().size())), 3);
            }
        }

        if (!scoreboard.containsScore(2)) {
            scoreboard.add(FontColor.translateString("&a"), 2);
        }

        if (!scoreboard.containsScore(1)) {
            scoreboard.add(FontColor.translateString("&6~~~~~~~~~~~~~~&b"), 1);
        }
    }

    public void updateEndGameScoreboard(EndGamePhase phase) {
        String title = FontColor.BOLD + "Starting In: " + ChatColor.GOLD + String.valueOf(phase.getCount());

        if (!title.equals(scoreboard.getTitle())) {
            scoreboard.setTitle(title);
        }

        if (!scoreboard.containsScore(5)) {
            scoreboard.add(FontColor.translateString("&6~~~~~~~~~~~~~~&a"), 5);
        }

        if (!scoreboard.containsScore(4)) {
            scoreboard.add(FontColor.translateString("&b&l> &e&lWinner:"), 4);
        }

        String winner = phase.getWinner() == null ? "n/a" : phase.getWinner().getName();
        if (!scoreboard.containsScore(3)) {
            scoreboard.add(FontColor.translateString("&f&o") + winner, 3);
        } else if (!scoreboard.get(3, null).contains(winner)) {
            scoreboard.remove(3, scoreboard.get(3, null));
            scoreboard.add(FontColor.translateString("&f&o") + winner, 3);
        }

        if (!scoreboard.containsScore(2)) {
            scoreboard.add(FontColor.translateString("&a"), 2);
        }

        if (!scoreboard.containsScore(1)) {
            scoreboard.add(FontColor.translateString("&6~~~~~~~~~~~~~~&b"), 1);
        }
    }
}
