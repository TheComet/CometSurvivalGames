package co.thecomet.sg.game.ui;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.minigameapi.world.LoadedMap;
import co.thecomet.minigameapi.world.MapManager;
import co.thecomet.sg.config.SGMapConfig;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class MapSelectionMenu extends Menu implements Runnable {
    private int taskId;
    private MenuItem item;
    private MapManager<SGMapConfig> manager;
    private Map<String, LoadedMap<SGMapConfig>> configs = new HashMap<>();
    private Map<UUID, String> votes = new HashMap<>();
    private Cache<UUID, Long> cooldown = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();
    
    public MapSelectionMenu(MapManager<SGMapConfig> manager) {
        super("Map Selection", 6);
        this.item = new MenuItem("Map Selection", new MaterialData(Material.COMPASS)) {
            @Override
            public void onClick(Player player) {
                if (manager.isUpdating()) {
                    openMenu(player);
                }
            }
        };
        this.manager = manager;
        this.taskId = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), this, 0, 20 * 10).getTaskId();
    }

    @Override
    public void run() {
        init();
    }
    
    private void init() {
        for (LoadedMap<SGMapConfig> map : manager.getMaps()) {
            if (!configs.containsKey(map.getMapName())) {
                configs.put(map.getMapName(), map);
                addMap(map, configs.size() - 1);
            }
        }

        updateInventory();
        updateMenu();
    }
    
    private void addMap(LoadedMap<SGMapConfig> map, int index) {
        MenuItem item = new MenuItem(map.getMapName(), new MaterialData(Material.MAP)) {
            @Override
            public void onClick(Player player) {
                if (votes.containsKey(player.getUniqueId())) {
                    if (votes.get(player.getUniqueId()).equalsIgnoreCase(map.getMapName())) {
                        MessageFormatter.sendErrorMessage(player, "You have already voted for that map!");
                        return;
                    }
                }

                Map<UUID, Long> cooldownMap = cooldown.asMap();
                if (cooldownMap.containsKey(player.getUniqueId())) {
                    MessageFormatter.sendErrorMessage(player, "You must wait " + ((cooldownMap.get(player.getUniqueId()) - System.currentTimeMillis()) / 1000) + " seconds before changing your vote.");
                    return;
                }
                
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (manager.isUpdating() == false) {
                        closeMenu(player);
                    }
                    
                    if (p == player) {
                        MessageFormatter.sendSuccessMessage(p, "You have " + (votes.containsKey(player.getUniqueId()) ? "changed your vote to &6" : "voted for &6") + map.getMapName());
                    } else {
                        MessageFormatter.sendInfoMessage(p, "&6" + player.getName() + " has " + (votes.containsKey(player.getUniqueId()) ? "changed their vote to &6" : "voted for &6") + map.getMapName());
                    }

                    votes.put(player.getUniqueId(), map.getMapName());
                }

                cooldown.put(player.getUniqueId(), System.currentTimeMillis() + 5000);
            }
        };
        item.setDescriptions(new ArrayList<String>() {{
            if (map.getMapAuthor() != null) {
                add(FontColor.YELLOW + "Author: " + map.getMapAuthor());
            }
        }});
        
        this.addMenuItem(item, index);
    }
    
    public LoadedMap<SGMapConfig> getWinningMap() {
        Map<LoadedMap<SGMapConfig>, Integer> mapCounts = new ConcurrentHashMap<>();
        for (String selection : votes.values()) {
            LoadedMap<SGMapConfig> map = configs.get(selection);
            mapCounts.put(map, mapCounts.containsValue(map) ? mapCounts.get(map) + 1 : 1);
        }
        
        LoadedMap<SGMapConfig> map = null;
        for (Map.Entry<LoadedMap<SGMapConfig>, Integer> entry : mapCounts.entrySet()) {
            if (map == null) {
                map = entry.getKey();
            } else {
                if (entry.getValue() > mapCounts.get(map)) {
                    map = entry.getKey();
                }
            }
        }
        
        if (map == null) {
            map = manager.getMaps().get(GeneralUtils.getBetween(0, manager.getMaps().size()));
        }
        
        return map;
    }
    
    public LoadedMap<SGMapConfig> getVotedMap(Player player) {
        return hasVoted(player) ? configs.get(votes.get(player.getUniqueId())) : null;
    }
    
    public boolean hasVoted(Player player) {
        return votes.containsKey(player.getUniqueId());
    }
    
    public void removeVote(Player player) {
        votes.remove(player.getUniqueId());
    }

    public MenuItem getItem() {
        return item;
    }

    public int getTaskId() {
        return taskId;
    }
}
