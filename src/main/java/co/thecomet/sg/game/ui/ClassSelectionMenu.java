package co.thecomet.sg.game.ui;

import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.sg.game.GameLoop;
import co.thecomet.sg.game.classes.*;
import co.thecomet.sg.game.phases.PreGamePhase;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Map;

public class ClassSelectionMenu extends Menu {
    private static Map<Class<? extends GameClass>, GameClass> gameClasses;
    private MenuItem item;
    
    public ClassSelectionMenu() {
        super("Class Selection", 6);
        gameClasses = GameLoop.getInstance().getGameClasses();
        item = new MenuItem("Class Selection", new MaterialData(Material.IRON_AXE)) {
            @Override
            public void onClick(Player player) {
                if (GameLoop.getInstance().getPhase() instanceof PreGamePhase) {
                    openMenu(player);
                }
            }
        };
        init();
    }
    
    private void init() {
        this.gameClasses.putIfAbsent(Swordsman.class, new Swordsman());
        this.gameClasses.putIfAbsent(Scout.class, new Scout());
        this.gameClasses.putIfAbsent(Tank.class, new Tank());
        this.gameClasses.putIfAbsent(Archer.class, new Archer());
        this.gameClasses.putIfAbsent(LuckyAxeMaster.class, new LuckyAxeMaster());
        this.gameClasses.putIfAbsent(SuperShepherd.class, new SuperShepherd());
        this.gameClasses.putIfAbsent(Beastmaster.class, new Beastmaster());
        this.gameClasses.putIfAbsent(Rider.class, new Rider());
        
        for (GameClass gameClass : new ArrayList<>(gameClasses.values())) {
            addMenuItem(gameClass.getMenuItem(), gameClass.getMenuItem().getSlot());
        }
    }

    public MenuItem getItem() {
        return item;
    }
}
