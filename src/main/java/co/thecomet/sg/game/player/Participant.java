package co.thecomet.sg.game.player;

import co.thecomet.core.config.JsonLocation;
import co.thecomet.sg.game.classes.GameClass;
import org.bukkit.Bukkit;

import java.util.UUID;

public class Participant {
    private UUID uuid;
    private String name;
    private GameClass gameClass = null;
    private JsonLocation pedestal = null;
    private boolean isAlive = true;
    
    public Participant(UUID uuid) {
        this.uuid = uuid;
        this.name = Bukkit.getPlayer(uuid).getName();
    }

    public GameClass getGameClass() {
        return gameClass;
    }

    public void setGameClass(GameClass gameClass) {
        this.gameClass = gameClass;
    }

    public JsonLocation getPedestal() {
        return pedestal;
    }

    public void setPedestal(JsonLocation pedestal) {
        this.pedestal = pedestal;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }
}
