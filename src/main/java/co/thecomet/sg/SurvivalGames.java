package co.thecomet.sg;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CorePlugin;
import co.thecomet.core.moderation.commands.TeleportCommands;
import co.thecomet.sg.config.SGConfig;
import co.thecomet.sg.game.GameLoop;
import co.thecomet.sg.game.phases.InProgressPhase;
import org.bukkit.Bukkit;

import java.io.File;
import java.util.List;

public class SurvivalGames extends CorePlugin {
    private static SGConfig config;
    
    @Override
    public void enable() {
        config = initConfig();
        Bukkit.getWorlds().get(0).setAutoSave(false);
        Bukkit.getScheduler().runTaskTimer(this, new GameLoop(this), 20, 20);
        new TeleportCommands(true, Rank.MOD, true, Rank.ADMIN, false, null, false, null, true, Rank.ADMIN);
    }

    @Override
    public void disable() {
        //
    }

    @Override
    public String getType() {
        return "SG";
    }

    @Override
    public String getStatus() {
        if (GameLoop.getInstance() != null && GameLoop.getInstance().getPhase() instanceof InProgressPhase) {
            return "PLAYING";
        }
        
        return super.getStatus();
    }

    @Override
    public List<Class<?>> getDBClasses() {
        return null;
    }
    
    public SGConfig initConfig() {
        File file = new File(getDataFolder(), "config.json");
        SGConfig config = JsonConfig.load(file, SGConfig.class);
        
        if (file.exists() == false) {
            config.save(file);
        }
        
        return config;
    }
    
    public static SGConfig getSGConfig() {
        return config;
    }
}
